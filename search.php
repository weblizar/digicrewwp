<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Digicrew
 */

get_header(); 
$sidebar_pos = digicrew_get_opt( 'archive_sidebar_pos', 'right' ); ?>
<!-- Single-Blog -->
<section class="same-section-spacing single-blog">
    <div class="container">
        <div class="row">
            <div <?php digicrew_primary_class( $sidebar_pos, '' ); ?>>
                <?php if ( have_posts() ) :
                    /* Start the Loop */
                    while ( have_posts() ) : the_post(); 
                        get_template_part( 'template-parts/content', '' ); 
                    endwhile ; 
                else:
                    get_template_part( 'template-parts/content', 'none' );
                endif ; ?>
                <!-- Pagination -->
                <?php digicrew_theme_pagination(); ?>
            </div>
            <?php if ( 'none' != $sidebar_pos ) : ?>
                <!-- Side-bar -->
                <div <?php digicrew_secondary_class( $sidebar_pos, '' ); ?>>
                    <?php get_sidebar(); ?> 
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<!-- End-Single-Blog -->
<?php get_footer();