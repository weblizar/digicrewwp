/*--------------------------------------------------
Project:        Digicrew
Version:        1.0
Author:         Company Name
-----------------------------------------------------

    JS INDEX
    ================================================
    * preloader js
    * slick nav 
    * slick icon slider
    * counter
    * testimonial slider
    * slick icon slider
    * bottom to top
    * Isotop with ImagesLoaded
    * ACCORDION WITH TOGGLE ICONS
    * Google-Map
    ================================================*/
    
    (function(jQuery) {
      "use strict";
  
      var $main_window = jQuery(window);
  
      /*====================================
      preloader js
      ======================================*/
      $main_window.on('load', function() {
          jQuery('#preloader').fadeOut('slow');
      });
  
      
      /*====================================
          slick nav
      ======================================*/
      var logo_path = jQuery('.mobile-menu').data('logo');
      jQuery('.navbar-nav').slicknav({
          appendTo: '.mobile-menu',
          removeClasses: true,
          label: '',
          closedSymbol: '<i class="fa fa-angle-right"><i/>',
          openedSymbol: '<i class="fa fa-angle-down"><i/>',
          brand: '<a href="#"><img src="' + logo_path + '" class="img-fluid"></a>'
      });
  
      /*====================================
          slick icon slider
      ======================================*/
      jQuery('.icon-slide').slick({
          infinite: true,
          dots: true,
          speed: 300,
          slidesToShow: 4,
          slidesToScroll: 4,
          responsive: [{
                  breakpoint: 1200,
                  settings: {
                      slidesToShow: 3,
                      slidesToScroll: 3,
                      infinite: true,
                      dots: true
                  }
              },
              {
                  breakpoint: 992,
                  settings: {
                      slidesToShow: 2,
                      slidesToScroll: 2
                  }
              },
              {
                  breakpoint: 600,
                  settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1
                  }
              }
              // You can unslick at a given breakpoint now by adding:
              // settings: "unslick"
              // instead of a settings object
          ]
      });
  
      /*====================================
          counter
      ======================================*/
  
      jQuery('.counter').counterUp({
          delay: 10,
          time: 1000
      });
      /*====================================
          testimonial slider
      ======================================*/
      jQuery('.slider-for').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          fade: true,
          asNavFor: '.slider-nav'
      });
      jQuery('.slider-nav').slick({
          slidesToShow: 5,
          slidesToScroll: 1,
          asNavFor: '.slider-for',
          dots: false,
          centerMode: true,
          focusOnSelect: true,
          responsive: [{
                  breakpoint: 1200,
                  settings: {
                      slidesToShow: 4,
                      slidesToScroll: 4,
                      infinite: true,
                      dots: true
                  }
              },
              {
                  breakpoint: 992,
                  settings: {
                      slidesToShow: 3,
                      slidesToScroll: 3
                  }
              },
              {
                  breakpoint: 767,
                  settings: {
                      slidesToShow: 2,
                      slidesToScroll: 2
                  }
              },
              {
                  breakpoint: 580,
                  settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1
                  }
              }
  
              // You can unslick at a given breakpoint now by adding:
              // settings: "unslick"
              // instead of a settings object
          ]
      });
  
      /*====================================
            slick icon slider
        ======================================*/
      jQuery('.port-slide').slick({
          infinite: true,
          dots: true,
          speed: 300,
          slidesToShow: 4,
          slidesToScroll: 4,
          responsive: [{
                  breakpoint: 1500,
                  settings: {
                      slidesToShow: 3,
                      slidesToScroll: 3,
                      infinite: true,
                      dots: true
                  }
              },
              {
                  breakpoint: 1200,
                  settings: {
                      slidesToShow: 2,
                      slidesToScroll: 2
                  }
              },
              {
                  breakpoint: 992,
                  settings: {
                      slidesToShow: 3,
                      slidesToScroll: 3
                  }
              },
              {
                  breakpoint: 767,
                  settings: {
                      slidesToShow: 2,
                      slidesToScroll: 2
                  }
              },
              {
                  breakpoint: 550,
                  settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1
                  }
              }
          ]
      });
      /*====================================
          bottom to top
      ======================================*/
      var btn = jQuery('#btn-to-top');
  
      jQuery(window).scroll(function() {
          if (jQuery(window).scrollTop() > 300) {
              btn.addClass('show');
          } else {
              btn.removeClass('show');
          }
      });
  
      btn.on('click', function(e) {
          e.preventDefault();
          jQuery('html, body').animate({
              scrollTop: 0
          }, '300');
      });
  
      /*====================================
          Isotop with ImagesLoaded
      ======================================*/
      var isotopFilter = jQuery('.isotop-filter');
      var isotopGrid = jQuery('.isotop-grid');
      var isotopGridMasonry = jQuery('.isotop-grid-masonry');
      var isotopGridItem = '.isotop-item';
      /*-- Images Loaded --*/
      isotopGrid.imagesLoaded(function() {
          /*-- Filter List --*/
          isotopFilter.on('click', 'button', function() {
              isotopFilter.find('button').removeClass('active');
              jQuery(this).addClass('active');
              var filterValue = jQuery(this).attr('data-filter');
              isotopGrid.isotope({
                  filter: filterValue
              });
          });
          /*-- Filter Grid Layout FitRows --*/
          isotopGrid.isotope({
              itemSelector: isotopGridItem,
              layoutMode: 'fitRows',
              masonry: {
                  columnWidth: '.isotop-grid',
              }
          });
          /*-- Filter Grid Layout Masonary --*/
          isotopGridMasonry.isotope({
              itemSelector: isotopGridItem,
              layoutMode: 'masonry',
              masonry: {
                  columnWidth: 1,
              }
          });
      });
      /*====================================
          ACCORDION WITH TOGGLE ICONS
      ======================================*/
      function toggleIcon(e) {
          jQuery(e.target)
              .prev('.panel-heading')
              .find(".more-less")
              .toggleClass('fa-angle-down fa-angle-up');
      }
      jQuery('.panel-group').on('hidden.bs.collapse', toggleIcon);
      jQuery('.panel-group').on('shown.bs.collapse', toggleIcon);



    jQuery(document).ready(function() {
        jQuery(".navbar-nav").accessibleDropDown();
    });

    jQuery.fn.accessibleDropDown = function () {
      var el = jQuery(this);

      /* Make dropdown menus keyboard accessible */

        jQuery("a", el).focus(function() {
            jQuery(this).parents("li").addClass("force-show");
        }).blur(function() {
            jQuery(this).parents("li").removeClass("force-show");
        });
    }

})(jQuery);
  
  /*------------------ Google-Map --------------------*/
  var iconmap = document.querySelectorAll('#map');
  var img_src = jQuery("#map").attr('data-icon');
  
  for (var i in iconmap)
      if (iconmap.hasOwnProperty(i)) {
  
          function initMap() {
              var map = new google.maps.Map(document.getElementById("map"), {
                  zoom: 10,
                  center: {
                      lat: 40.7819502,
                      lng: -74.7357194
                  }
              });
              var map_icon = img_src;
              var marker = new google.maps.Marker({
                  position: map.getCenter(),
                  icon: map_icon,
                  map: map
              });
          }
      }