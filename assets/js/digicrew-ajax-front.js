var window, document, jQuery;
(function (jQuery) {
    "use strict"; 

    var sticky_on = ajax_admin.ajax_data.sticky_on;
    
    if ( ajax_admin.ajax_data.sticky_on == true ) {
        /*====================================
        sticky menu js
        ======================================*/
        var windows = jQuery(window);
        var sticky = jQuery('.header-one')
        windows.on('scroll', function() {
            var scroll = windows.scrollTop();
            if (scroll < 50) {
                sticky.removeClass('stick');
            } else {
                sticky.addClass('stick');
            }
        });
    } 
})(jQuery);