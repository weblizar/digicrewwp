<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Digicrew
 */
get_header(); 
$show_sidebar_page = digicrew_get_page_opt( 'show_sidebar_page', false );

if ( $show_sidebar_page ) {
    $sidebar_pos   = digicrew_get_page_opt( 'sidebar_page_pos' );
} else {
    $sidebar_pos   = '';
}
?>
<section class="same-section-spacing single-blog page-box" id="content">
    <div class="container">
    	<div class="row">
	    	<div <?php digicrew_primary_class( $sidebar_pos, '' ); ?>>
				<?php
				while ( have_posts() ) : the_post(); ?>
					
						<?php 
						the_content();
						wp_link_pages( 
							array(
								'before'    => '<nav><ul class="pagination custom-nav">',
								'after'     => '</ul></nav>',
								'separator' => ' ',
							) 
						);
						?>
					
					<?php 
					if ( comments_open() || get_comments_number() ) { ?>
						<?php comments_template();?>
					<?php } 
				endwhile; // End of the loop.
				?>	
			</div>
			<?php if ( '1' == $show_sidebar_page && ! is_front_page() ) : ?>
		        <div <?php digicrew_secondary_class( $sidebar_pos, '' ); ?>>
		            <?php get_sidebar(); ?>  
		        </div>
		    <?php endif; ?>
    	</div>
    </div>
</section>
<?php get_footer(); ?>