<?php 
/**
 * Portfolio Post.
 *
 * @package Digicrew
 * @since   1.0.0
*/
global $post;

get_header();
the_post();
$project_website_url = sanitize_text_field( get_post_meta( get_the_ID(), 'project_website_url', true ) );
$project_sd          = stripslashes( get_post_meta( get_the_ID(), 'project_sd', true ) );
$project_skill       = sanitize_text_field( get_post_meta( get_the_ID(), 'project_skill', true ) );
$project_createdby   = sanitize_text_field( get_post_meta( get_the_ID(), 'project_createdby', true ) );
$project_email       = sanitize_text_field( get_post_meta( get_the_ID(), 'project_email', true ) );
$date                = sanitize_text_field( get_post_meta( get_the_ID(), 'date', true ) );
$project_wrktyp      = sanitize_text_field( get_post_meta( get_the_ID(), 'project_wrktyp', true ) );
?>
<!-- Case-listing -->
<section class="same-section-spacing single-port" id="content">
    <div class="container">
        <div class="row">
        	<?php if ( has_post_thumbnail() ) { ?>
	            <div class="col-lg-6 col-md-12">
	                <div class="img-wapper">
	                    <?php echo the_post_thumbnail( '', array( 'class' => 'img-responsive' ) ); ?>
	                </div>
	            </div>
	        <?php } ?>
            <div class="<?php if ( has_post_thumbnail() ) { ?> col-lg-6 <?php } else { ?> col-lg-12 <?php } ?> col-md-12">
                <div class="project-info">
                    <h3 class="title"><?php esc_html_e( 'Project Information', 'digicrew' ); ?></h3>
                    <ul>
                    	<?php if ( $project_skill !== "" ) { ?>
                        	<li>
                                <?php esc_html_e( 'Skill Needs', 'digicrew' ); ?>
                                <span> <?php echo esc_html( $project_skill ); ?> </span>
                            </li>
                        <?php } if ( $project_createdby !== "" ) { ?>
                        	<li>
                                <?php esc_html_e( 'Create By', 'digicrew' ); ?>
                                <span> <?php echo esc_html( $project_createdby ); ?> </span>
                            </li>
                        <?php } if ( $project_email !== "" ) { ?>
                        	<li>
                                <?php esc_html_e( 'Email', 'digicrew' ); ?>
                                <span> <?php echo esc_html( $project_email ); ?> </span>
                            </li>
                        <?php } if ( $date !== "" ) { ?>
                        	<li>
                                <?php esc_html_e( 'Date', 'digicrew' ); ?>
                                <span> <?php echo esc_html( date( 'd-m-Y', strtotime( $date ) ) ); ?> </span>
                            </li>

                        <?php } if ( $project_wrktyp !== "" ) { ?>
                        	<li>
                                <?php esc_html_e( 'Type Of Work ', 'digicrew' ); ?>
                                <span><?php echo esc_html( $project_wrktyp ); ?></span>
                            </li>
                        <?php } if ( $project_website_url !== "" ) { ?>
                        	<li>
                                <?php esc_html_e( 'Live Project', 'digicrew' ); ?> 
                                <span> <a href="<?php echo esc_url( $project_website_url ); ?>"><?php echo esc_html( $project_website_url ); ?></a> </span>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <?php if ( $project_sd !== "" ) { ?>
	            <div class="col-lg-12">
	                <div class="pro-diss">
	                    <h2><?php esc_html_e( 'Project Description', 'digicrew' ); ?></h2>
	                    <p><?php echo get_post_meta( get_the_ID(), 'project_sd', true ) ; ?></p>
	                </div>
	            </div>
	        <?php } ?>
	    </div>
        <?php the_content(); ?>
    </div>
</section>
<!-- End-Case-listing -->
<?php get_footer(); ?>
<!-- ====== Recent Work End ====== -->