<?php
/**
 * Digicrew functions and definitions
 * @package Digicrew
*/

// Initialize Theme
require get_template_directory() . '/include/theme-setup.php';

// Custom template tags for this theme.
require get_template_directory() . '/include/template-tags.php';

// Functions which enhance the theme by hooking into WordPress.
require get_template_directory() . '/include/template-functions.php';

// Theme Options.
require get_template_directory() . '/include/theme-options.php';

//Navwalker.
require get_template_directory() . '/include/digicrew_custom_nav_walker.php';

// Page Options.
require get_template_directory() . '/include/page-options.php';

/* Load lib Font */
require_once get_template_directory() . '/include/libs/fontawesome.php';
require_once get_template_directory() . '/include/libs/flaticon.php';

function digicrew_vc_after_init() {
 require_once ( get_template_directory() . '/vc_elements/digicrew_custom_vc_param.php' );
}
add_action('vc_after_init', 'digicrew_vc_after_init');

// CSS Generator.
if (!class_exists('CSS_Generator')) {
    require_once get_template_directory() . '/include/classes/class-css-generator.php';
}

/**
 * Functions which enhance the theme by hooking into WordPress.
*/
require_once get_template_directory() . '/include/extends.php';