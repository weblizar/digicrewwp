<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Digicrew
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) { return; }
$post_comments_form_on = digicrew_get_opt( 'post_comments_form_on', true );
if ( $post_comments_form_on ) : ?>
	<div class="comment-area">
		<?php
		// You can start editing here -- including this comment!
		if ( have_comments() ) :
			?><!-- .comments-title -->
			<h3 class="title">
				<?php echo comments_number('No Comments', '1 Comment', '% Comments'); ?>
				
			</h3><!-- .comments-title -->
			
			<?php the_comments_navigation(); ?>
			<ul class="comment-box">
				<?php
					wp_list_comments( array(
						'style'       => 'ul',
						'short_ping'  => true,
						'callback'    => 'digicrew_comment_list',
						'avatar_size' => 100,
					) );
				?>
			</ul><!-- .comment-list -->

			<?php the_comments_navigation();

			// If comments are closed and there are comments, let's leave a little note, shall we?
			if ( ! comments_open() ) : ?>
				<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'digicrew' ); ?></p> 
			<?php endif; ?>
			
		<?php endif; // Check for have_comments().
		?>
	</div><!-- #comments -->
	<div class="comment-form">
		<?php 
		$args = array(
			'id_form'              => 'commentform',
			'id_submit'            => 'submit',
			'title_reply'          => esc_html__( 'Comments', 'digicrew' ),
			'title_reply_to'       => esc_html__( 'Leave A Reply To ', 'digicrew' ) . '%s',
			'cancel_reply_link'    => esc_html__( 'Cancel Reply', 'digicrew' ),
			'label_submit'         => esc_html__( 'Submit', 'digicrew' ),
			'title_reply_before'   => '<h3 class="title">',
			'title_reply_after'    => '</h3>',
			'comment_notes_before' => '',
			'fields'               => apply_filters( 'comment_form_default_fields', 
				array(
					'author' =>
					'<div class="row"><div class="col-lg-6 col-md-12"><div class="form-group">'.
					'<input id="author" class="form-control" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
					'" size="30" placeholder="'.esc_attr__( 'Name', 'digicrew' ).'"/><span class="form-icon fas fa-pencil-alt"></span></div></div>',

					'email' =>
					'<div class="col-lg-6 col-md-12"><div class="form-group">'.
					'<input id="email" class="form-control" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) .
					'" size="30" placeholder="'.esc_attr__( 'Email', 'digicrew' ).'"/><span class="form-icon fas fa-envelope"></span></div></div></div>',
				)
			),
			'comment_field' =>  '
			<textarea id="comment" class="form-control" name="comment" cols="45" rows="5" placeholder="'.esc_attr__( 'Your Comment', 'digicrew' ).'" aria-required="true">' .
			'</textarea>',
			'submit_button' => '<input type="submit" class="btn" id="submit" value="Post Comment">',
		); 
		comment_form( $args ); ?>
	</div>
<?php endif; ?>