<?php
/**
 * The template for displaying the footer
 * @package Digicrew
 */
?>
<!-- Footer-section -->
<footer class="footer-bg">
    <?php if ( is_active_sidebar( 'digicrew-footer1') || is_active_sidebar( 'digicrew-footer2' ) || is_active_sidebar( 'digicrew-footer3' ) || is_active_sidebar( 'digicrew-footer3' ) || is_active_sidebar( 'digicrew-footer4' ) || is_active_sidebar( 'digicrew-footer5' ) ) { ?>
        <div class="ws-section-spacing">
            <div class="container">
                <div class="row">
                    <?php if ( is_active_sidebar( 'digicrew-footer1') ) { ?>
                        <div class="<?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?> col-lg-<?php echo esc_attr( digicrew_get_opt( 'footer-select-col' ) ); } ?> col-md-3 col-sm-6">
                            <?php dynamic_sidebar( 'digicrew-footer1' ); ?>
                        </div>
                    <?php } ?>
                    <?php if( is_active_sidebar( 'digicrew-footer2' ) ) { ?>
                        <div class="<?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>col-lg-<?php echo esc_attr( digicrew_get_opt( 'footer-select-col' ) ); } ?> col-md-3 col-sm-6">
                            <?php dynamic_sidebar('digicrew-footer2' ); ?>
                        </div>

                    <?php } if( is_active_sidebar( 'digicrew-footer3' ) ) { ?>
                        <div class="<?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?> col-lg-<?php echo esc_attr( digicrew_get_opt( 'footer-select-col' ) ); } ?> col-md-3 col-sm-6">
                            <?php dynamic_sidebar('digicrew-footer3' ); ?>
                        </div>
                    <?php } if( is_active_sidebar( 'digicrew-footer4' ) ) { ?>
                        <div class="<?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?> col-lg-<?php echo esc_attr( digicrew_get_opt( 'footer-select-col' ) ); } ?> col-md-3 col-sm-6">
                            <?php dynamic_sidebar( 'digicrew-footer4' ); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="copyright-area">
        <div class="container">
            <div class="row">
                <div class="<?php if ( has_nav_menu( 'footer' ) ) { ?> col-lg-6 <?php } else { ?> col-lg-12 text-center <?php } ?>">
                    <?php if( digicrew_get_opt( 'footer_copyright') ) : 
                        print wp_kses( digicrew_get_opt( 'footer_copyright' ) , wp_kses_allowed_html() ); 
                    else : ?> 
                        <p>
                            <?php
                                /* translators: 1: Theme name, 2: Theme author. */
                                printf( esc_html__( '&copy; 2020 Landing. All rights reserved.', 'digicrew' ) );
                            ?>
                        </p>
                    <?php endif ; ?> 
                </div>
                <?php if ( has_nav_menu( 'footer' ) ) : ?>
                    <div class="col-lg-6 text-right">
                        <?php 
                            wp_nav_menu( array (
                                'container'      => 'ul',
                                'theme_location' => 'footer',
                                'before'         => '',
                                'items_wrap'     => '<ul class="copyright-nav">%3$s</ul>',
                                'depth'          => 1,
                                'fallback_cb'    => false,
                                'walker'         =>  new digicrewCustomNavWalker()
                                ) 
                            );
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</footer>
<!-- End-footer-section -->
<?php if ( digicrew_get_opt( 'gototop', false ) ) : ?>	
	<!-- Back to top button -->
    <a id="btn-to-top"></a>
<?php endif ; ?>
<?php wp_footer(); ?>
</body>
</html>