<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Digicrew
*/ 
?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="blog-box blog-single-box <?php if ( has_post_thumbnail() ) :?>blog-img<?php endif ; ?>">
        <?php if ( has_post_thumbnail() ) : ?>
            <div class="wapper-img">
                <?php the_post_thumbnail( array( 570, 365 ) ); ?>
            </div>
        <?php endif ; ?>
        <div class="blog-content">
            <?php digicrew_archive_meta(); ?>
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            
            <p><?php echo digicrew_custom_excerpt( 45 ); ?></p>
            <a href="<?php the_permalink(); ?>" class="read-btn">
                <?php esc_html_e( 'Read More', 'digicrew' );?> <i class="fas fa-angle-double-right"></i>
            </a>
        </div>
    </div>
</div>