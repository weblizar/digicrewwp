<?php  $h_menu = digicrew_get_page_opt( 'h_menu', 1 );?>
<!-- Header-start -->
<header class="header-one <?php if ( $h_menu == '0') { ?> dark <?php } ?>">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <?php 
                $logo          = digicrew_get_opt( 'logo', array( 
                    'url' => '', 
                    'id' => '' 
                ) );
                $logo_url      = $logo['url'];
                $custom_header = digicrew_get_page_opt( 'custom_header', '0');
                if ( is_page() && $custom_header == '1' ) {
                    $page_logo = digicrew_get_page_opt('page_logo', array( 
                        'url' => '', 
                        'id' => '' 
                    ));
                    $logo_url  = $page_logo['url'];
                    
                    if ( $logo_url == '0' ) {
                        return;
                    }
                }
                if ( $logo_url ) : ?>
                   <img src="<?php echo esc_url( $logo_url );  ?>" alt="<?php bloginfo( 'name' ); ?>">
                <?php else : ?>
                    <h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
                <?php endif; ?>
            </a>

            <div class="collapse navbar-collapse my-lg-0" id="navbarNav">
                <?php 
                if ( has_nav_menu( 'primary' ) ) :
                    wp_nav_menu( array (
                      'container'      => 'ul',
                      'theme_location' => 'primary',
                      'before'         => '',
                      'items_wrap'     => '<ul class="navbar-nav">%3$s</ul>',
                      'walker'         =>  new digicrewCustomNavWalker()
                    ) 
                );
                else :
                    printf(
                      '<ul class="navbar-nav"><li class="nav-item"><a class="nav-link" href="%1$s">%2$s</a></li></ul>',
                      esc_url( admin_url( 'nav-menus.php' ) ),
                      esc_html( 'Create New Menu', 'digicrew' )
                    );
                endif;
                $btn_txt = digicrew_get_opt( 'btn_txt');
                if ( $btn_txt != '' ) { ?>
                    <div class="log-btn">
                        <a href="<?php echo esc_url( digicrew_get_opt( 'btn_url') ); ?>"><?php echo esc_html( digicrew_get_opt( 'btn_txt') ); ?></a>
                    </div>
                <?php } ?>
            </div>
        </nav>
        <?php if ( $logo_url ) { ?>
            <div class="mobile-menu" data-logo="<?php echo esc_url( $logo_url );  ?>">
            </div>
        <?php } else{ ?>
            <div class="mobile-menu" data-logo="<?php echo esc_attr( get_template_directory_uri() ) . '/assets/images/logo.png'; ?>">
            </div>
        <?php } ?>
    </div>
</header>
<!-- Header-end -->