<?php
$titles = digicrew_get_page_titles();
ob_start();
if ( $titles['title'] ) {
    printf( '<h2>%s</h2>', esc_attr( $titles['title'] ) );
}
$titles_html    = ob_get_clean();
$ptitle_on      = digicrew_get_opt( 'page_title_on', 'show' );
$page_ptitle_on = digicrew_get_page_opt( 'ptitle_on', 'themeoption' );

if(is_page() && !empty($page_ptitle_on) && $page_ptitle_on != 'themeoption' ) {
    $ptitle_on = $page_ptitle_on;
} 
$page_subtitle = digicrew_get_opt( 'page_subtitle' );
$p_subtitle    = digicrew_get_page_opt( 'p_subtitle', 'themeoption' );
if ( is_page() && !empty ( $p_subtitle ) ) {
    $page_subtitle = $p_subtitle;
} 
?>
<?php if( $ptitle_on == 'show' ) : ?>
    <!-- Banner-saction -->
    <section class="banner-bg same-section-spacing">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="inner-banner-info pt-5">
                        <?php printf( '%s', wp_kses( $titles_html, array( 'h2' => array() ) ) ); ?>
                        <?php if(!empty($page_subtitle)){ ?>
                        <p><?php echo esc_html( $page_subtitle ); ?></p>
                        <?php } ?>
                    </div>
                </div>

            </div>
            <nav aria-label="breadcrumb" class="breadcrumb-right">
                <ol class="breadcrumb">
                    <?php if ( function_exists('digicrew_breadcrumbs') ) digicrew_breadcrumbs(); ?>
                </ol>
            </nav>
        </div>
    </section>
    <!-- End-banner-section -->
<?php endif; ?>