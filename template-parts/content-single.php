<?php global $post; 
$post_social_share_on  = digicrew_get_opt( 'post_social_share_on', false );
$post_feature_image_on = digicrew_get_opt( 'post_feature_image_on', true );
?>
<div class="blog-box">
    <?php if( has_post_thumbnail() && $post_feature_image_on ) : ?>
        <div class="wapper-img wapsingleimg">
            <?php the_post_thumbnail('full'); ?>
        </div>
    <?php endif; ?>
    <div class="blog-content">
        <?php digicrew_post_meta(); ?>
        
        
        <div class="blog-data">
            <?php the_content();
            wp_link_pages( 
                array(
                    'before'    => '<nav><ul class="pagination custom-nav">',
                    'after'     => '</ul></nav>',
                    'separator' => ' ',
                ) 
            );
            
            if($post_social_share_on) : 
                do_action( 'digicrew_socials_share_html' );
            endif; ?>
        </div>
    </div>
</div>
<?php 
    // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) :
        comments_template();
    endif;
?>