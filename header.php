<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 * @package Digicrew
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php
	if ( ! function_exists( 'wp_body_open' ) ) {
	        function wp_body_open() {
	                do_action( 'wp_body_open' );
	        }
	}?>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'digicrew' ); ?></a>
	<?php if( digicrew_get_opt( 'show_page_loading', false ) ) : ?>
		<div id="preloader"></div>
	<?php endif ; 
	digicrew_header_layout(); 
	digicrew_page_title_layout();