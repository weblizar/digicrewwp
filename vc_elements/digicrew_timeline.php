<?php
$args = array(
    'name'        => esc_html__('Timeline','digicrew' ),
    'base'        => 'digicrew_timeline',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Timeline Displayed', 'digicrew' ),
    'category'    => esc_html__('Digicrew Shortcodes', 'digicrew'),
    'params'      => array(
        array(
            'type'       => 'param_group',
            'heading'    => esc_html__( 'Timeline Item', 'digicrew' ),
            'value'      => '',
            'param_name' => 'timeline_item',
            'params'     => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Title', 'digicrew'),
                    'param_name'  => 'title',
                    'admin_label' => true,
                ),
                array(
                    'type'       => 'textarea',
                    'heading'    => esc_html__('Content', 'digicrew'),
                    'param_name' => 'content',
                ),
            ),
        ),
        
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name'  => 'el_class',
            'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
        ),
    )
);

vc_map($args);  

class WPBakeryShortCode_digicrew_timelinne extends DSShortCode {

    protected function content($atts, $content = null) {
        return parent::content($atts, $content);
    }
}