<?php
vc_map( array(
    'name'        => esc_html__( 'Video Player','digicrew' ),
    'base'        => 'digicrew_video_player',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Embed Youtube/Vimeo player','digicrew' ),
    'category'    => esc_html__( 'Digicrew Shortcodes', 'digicrew' ),
    'params'      => array(

        array(
            'type'        => 'vc_link',
            'heading'     => esc_html__( 'Video Url', 'digicrew' ),
            'param_name'  => 'video_link',
            'value'       => '',
            'description' => esc_html__( 'Video url on Youtube, Vimeo' ,'digicrew')
        ),

        array(
            'type'       => 'attach_image',
            'heading'    => esc_html__( 'Video Background Image', 'digicrew' ),
            'param_name' => 'video_bg_image',
            'value'      => '',
        ),

        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name'  => 'el_class',
            'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
            'group'       => esc_html__( 'Extra', 'digicrew' )
        ),
    )
) );

class WPBakeryShortCode_digicrew_video_player extends DSShortCode
{
    protected function content($atts, $content = null)
    {
        return parent::content($atts, $content);
    }
}