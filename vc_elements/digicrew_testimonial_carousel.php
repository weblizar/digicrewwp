<?php
$args = array(
    'name'        => esc_html__('Testimonial Carousel','digicrew'),
    'base'        => 'digicrew_testimonial_carousel',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Testimonial Displayed', 'digicrew' ),
    'category'    => esc_html__('Digicrew Shortcodes', 'digicrew'),
    'params'      => array(
        array(
            'type'       => 'param_group',
            'heading'    => esc_html__( 'Testimonial Item', 'digicrew' ),
            'value'      => '',
            'param_name' => 'testimonial_item',
            'params'     => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Title', 'digicrew'),
                    'param_name'  => 'title',
                    'admin_label' => true,
                ),
                array(
                    'type'       => 'textfield',
                    'heading'    =>  esc_html__('Position', 'digicrew'),
                    'param_name' => 'position',

                ),
                array(
                    'type'       => 'textarea',
                    'heading'    => esc_html__('Content', 'digicrew'),
                    'param_name' => 'content',
                ),
                array(
                    'type'        => 'attach_image',
                    'heading'     => esc_html__( 'Image', 'digicrew' ),
                    'param_name'  => 'image',
                    'value'       => '',
                    'description' => esc_html__( 'Select image from media library.', 'digicrew' ),
                ),
            ),
        ),
        
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name'  => 'el_class',
            'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
        ),
    )
);

vc_map($args);  

class WPBakeryShortCode_digicrew_testimonial_carousel extends DSShortCode {

    protected function content($atts, $content = null) {
        return parent::content($atts, $content);
    }
}