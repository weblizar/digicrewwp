<?php
vc_map(array(
    'name'        => esc_html__( 'Service', 'digicrew' ),
    'base'        => 'digicrew_service',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Service Displayed', 'digicrew' ),
    'category'    => esc_html__( 'Digicrew Shortcodes', 'digicrew' ),
    'params'      => array(

        /* Style */
        array(
            'type'       => 'dropdown',
            'heading'    => esc_html__( 'Style', 'digicrew' ),
            'param_name' => 'style',
            'description'=> 'Select Style.',
            'value'      => array(
                esc_html__( 'Style 1', 'digicrew' ) => 'style-1',
                esc_html__( 'Style 2', 'digicrew' ) => 'style-2',
                esc_html__( 'Style 3', 'digicrew' ) => 'style-3',
            ),
        ),

        /* Title */
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Title', 'digicrew' ),
            'param_name' => 'title',
            'description'=> esc_html__( 'Enter title.','digicrew' ),
            'value'      => '',
        ),

        /* Description */
        array(
            'type'       => 'textarea',
            'heading'    => esc_html__( 'Description', 'digicrew' ),
            'param_name' => 'description',
            'description'=> esc_html__( 'Enter description.', 'digicrew' ),
            'value'      => '',
        ),

        /* Icon */
        array(
            'type'      => 'dropdown',
            'heading'   => esc_html__( 'Icon Library', 'digicrew' ),
            'value'     => array(
                esc_html__( 'Font Awesome', 'digicrew' ) => 'fontawesome',
                esc_html__( 'Flaticon', 'digicrew' )     => 'flaticon',
            ),
            'param_name'       => 'icon_list',
            'description'      => esc_html__( 'Select icon library.', 'digicrew' ),
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),
        array(
            'type'       => 'iconpicker',
            'heading'    => esc_html__( 'Icon FontAwesome', 'digicrew' ),
            'param_name' => 'icon_fontawesome',
            'value'      => '',
            'settings'   => array(
                'emptyIcon'    => true,
                'type'         => 'fontawesome',
                'iconsPerPage' => 200,
            ),
            'dependency'  => array(
                'element' => 'icon_list',
                'value'   => 'fontawesome',
            ),
            'description'      => esc_html__( 'Select icon from library.', 'digicrew' ),
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),  
        array(
            'type'       => 'iconpicker',
            'heading'    => esc_html__( 'Flaticon', 'digicrew' ),
            'param_name' => 'icon_flaticon',
            'settings'   => array(
                'emptyIcon'    => true,
                'type'         => 'flaticon',
                'iconsPerPage' => 200,
            ),
            'dependency'  => array(
                'element' => 'icon_list',
                'value'   => 'flaticon',
            ),
            'default'          => 'flaticon-system',
            'description'      => esc_html__( 'Select icon from library.', 'digicrew' ),
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),

        /* Style */
        array(
            'type'        => 'dropdown',
            'heading'     => esc_html__( 'Icon Background Color', 'digicrew' ),
            'param_name'  => 'icon_bg',
            'description' => 'Select Color.',
            'value'       => array(
                esc_html__( 'Pink', 'digicrew' )   => 'pink',
                esc_html__( 'Blue', 'digicrew' )   => 'blue',
                esc_html__( 'Orange', 'digicrew')  => 'red',
                esc_html__( 'Sky', 'digicrew' )    => 'sky',
                esc_html__( 'Green', 'digicrew' )  => 'green',
                esc_html__( 'Yellow', 'digicrew' ) => 'yallow',
            ),
        ),
        
        /* Extra */
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name'  => 'el_class',
            'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
        ),
    )
));

class WPBakeryShortCode_digicrew_service extends DSShortCode
{
    protected function content($atts, $content = null)
    {
        return parent::content($atts, $content);
    }
}