<?php
$args = array(
    'name'        => esc_html__( 'Custom Heading Theme' ,'digicrew' ),
    'base'        => 'digicrew_heading',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Heading Displayed', 'digicrew' ),
    'category'    => esc_html__( 'Digicrew Shortcodes', 'digicrew' ),
    'params'      => array(

        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => esc_html__( "Align", 'digicrew' ),
            "param_name"  => "style",
            "value"       => array (
                "Center"  => "center", 
                "Left"    => "left", 
                "Right"   => "right", 
            ),
            'admin_label' => true,
        ),

        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => esc_html__( "Color", 'digicrew' ),
            "param_name"  => "color",
            "value"       => array (
                "Dark"    => "dark", 
                "Light"   => "text-wht", 
            ),
            'admin_label' => true,
        ),
        
        array(
            "type"       => "textfield",
            "class"      => "",
            "heading"    => esc_html__( "Title", 'digicrew' ),
            "param_name" => "title",
        ),
        
        array(
            "type"        => "textfield",
            "class"       => "",
            "heading"     => esc_html__( "Description", 'digicrew' ),
            "param_name"  => "description",
        ),
        
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name'  => 'el_class',
            'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
        ),
    )
);

vc_map($args);  

class WPBakeryShortCode_digicrew_heading extends DSShortCode {
    protected function content($atts, $content = null) {
        return parent::content($atts, $content);
    }
} 