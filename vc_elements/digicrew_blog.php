<?php

$categories_array = array();
$args             = array('taxonomy' => 'category');
$categories       = get_categories( $args );
foreach ( $categories as $category ) {
    $categories_array[ $category->name ] = $category->slug;
}

$args = array(
    'name'        => esc_html__( 'Blog', 'digicrew' ),
    'base'        => 'digicrew_blog',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Blog Displayed', 'digicrew' ),
    'category'    => esc_html__( 'Digicrew Shortcodes', 'digicrew' ),
    'params'      => array(

        /* Column */
        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__( "Blog number of columns", 'digicrew' ),
            "param_name" => "bg_column",
            "description"=> esc_html__( "Choose the number of columns", 'digicrew' ),
            "value"      => array (
                esc_html__( 'Standard', 'digicrew' )      => "col-lg-12" ,
                esc_html__( 'Two columns', 'digicrew' )   => "col-lg-6 col-md-12 col-sm-12",
                esc_html__( 'Three columns', 'digicrew' ) => "col-lg-4 col-md-6 col-sm-12",
            ),
           
        ),

        array(
            "type"       => "checkbox",
            "class"      => "",
            "heading"    => esc_html__( "Category", 'digicrew' ),
            "param_name" => "category",
            'value'      => $categories_array,
            "description"=> esc_html__( "Select the Category to show / Filter By Category(optional)", 'digicrew' )
        ),

        array(
            "type"        => "textfield",
            "class"       => "",
            "heading"     => esc_html__( "Number of post to Show", 'digicrew' ),
            "param_name"  => "to_show",
            "value"       => ' ',
            "description" => esc_html__( "Number of post to Show", 'digicrew' )
        ),

        array(
            "type"       => "textfield",
            "class"      => "",
            "heading"    => esc_html__( "Excerpt Length", 'digicrew' ),
            "param_name" => "excerpt_length",
            "value"      => esc_html__( "50", 'digicrew' ),
            "description"=> esc_html__( "Number of character for blog post excerpt", 'digicrew' )
        ),

        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__( "Order", 'digicrew' ),
            "param_name" => "blog_order",
            "value"      => array ( 
                "Descending" => "DESC", 
                "Ascending" => 'ASC' 
            ),
            "description"=> esc_html__( "Choose the Order ( default Descending )", 'digicrew' ),
        ),

        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => esc_html__( "Order By", 'digicrew' ),
            "param_name"  => "blog_orderby",
            "value"       => array ( 
                "Date" => "date", 
                "Title" => 'title', 
                "Random" => 'rand', 
                "Author"  => 'author', 
                "ID" => 'ID', 
                "Modified" => 'modified' 
            ),
            "description" => esc_html__( "Choose by which parameter you want to order the posts ( default date )", 'digicrew' ),
        ),

        array(
            'type'       => 'checkbox',
            'heading'    => esc_html__( "Enable Pagination?", 'digicrew' ),
            'param_name' => 'pagination',
            'value'      => array(  'Yes'  => 'yes' ),
            'description'=> esc_html__( "Select if you want to use pagination for the blog", 'digicrew' ),
        ),

        array(
            'type'       => 'checkbox',
            'heading'    => esc_html__( "Show Blog Excerpt?", 'digicrew'),
            'param_name' => 'excerpt',
            'value'      => array(  'Yes'  => 'yes' ),
            'description'=> esc_html__( "Select if you want to use description for the blog", 'digicrew' ),
        ),

        array(
            'type'       => 'checkbox',
            'heading'    => esc_html__( "Show Read More?", 'digicrew'),
            'param_name' => 'readmore',
            'value'      => array(  'Yes'  => 'yes' ),
            'description'=> esc_html__( "Select if you want to use read more for the blog", 'digicrew' ),
        ),

        array(
            "type"        => "textfield",
            "class"       => "",
            "heading"     => esc_html__( "Extra Css", 'digicrew'),
            "param_name"  => "el_class",
            "value"       => '',
            "description" => esc_html__( "Enter Css Class For extra Blog Style ( Design )", 'digicrew' ),
        ),
    )
);

vc_map($args);  
class WPBakeryShortCode_digicrew_blog extends DSShortCode {
    protected function content( $atts, $content = null) {
        return parent::content( $atts, $content );
    }
}