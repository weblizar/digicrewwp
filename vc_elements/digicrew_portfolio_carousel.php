<?php
$categories_array = array();
$args             = array( 'taxonomy' => 'catportfolio' );
$categories       = get_categories( $args );
foreach ( $categories as $category ) {
    $categories_array[ $category->name ] = $category->slug;
}

$args = array(
    'name'        => esc_html__( 'Portfolio Carousel', 'digicrew' ),
    'base'        => 'digicrew_portfolio_carousel',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Portfolio Carousel Displayed', 'digicrew' ),
    'category'    => esc_html__( 'Digicrew Shortcodes', 'digicrew' ),
    'params'      => array(

        array(
            "type"       => "checkbox",
            "class"      => "",
            "heading"    => esc_html__( "Category", 'digicrew' ),
            "param_name" => "category",
            'value'      => $categories_array,
            "description"=> esc_html__( "Select the Category to show / Filter By Category(optional)", 'digicrew' )
        ),

        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__( "Order", 'digicrew' ),
            "param_name" => "port_order",
            "value"      => array ( 
                "Descending" => "DESC", 
                "Ascending" => 'ASC' 
            ),
            "description"=> esc_html__( "Choose the Order ( default Descending )", 'digicrew' ),
        ),
        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__( "Order By", 'digicrew' ),
            "param_name" => "port_orderby",
            "value"      => array ( 
                "Date" => "date", 
                "Title" => 'title', 
                "Random" => 'rand', 
                "Author"  => 'author', 
                "ID" => 'ID', 
                "Modified" => 'modified' 
            ),
            "description"=> esc_html__( "Choose by which parameter you want to order the posts ( default date )", 'digicrew' ),
        ),
            
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name' => 'el_class',
            'description'=> esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
            'group'      => esc_html__( 'Extra', 'digicrew' )
        ),

    ));

vc_map($args);

class WPBakeryShortCode_digicrew_portfolio_carousel extends DSShortCode
{

    protected function content($atts, $content = null) {
        return parent::content($atts, $content);
    }
}