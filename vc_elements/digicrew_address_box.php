<?php
vc_map(array(
    'name'        => esc_html__( 'Address Box', 'digicrew' ),
    'base'        => 'digicrew_address_box',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Address Box Displayed', 'digicrew' ),
    'category'    => esc_html__( 'Digicrew Shortcodes', 'digicrew' ),
    'params'      => array(

       
        /* Email. */
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Email Title', 'digicrew' ),
            'param_name'  => 'email_title',
            'description' => esc_html__( 'Enter Email Title.', 'digicrew' ),
            'value'       => esc_html__( 'EMAIL', 'digicrew' ),
        ),
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Email Text', 'digicrew' ),
            'param_name' => 'email_text',
            'description'=> esc_html__( 'Enter Email Text.', 'digicrew' ),
            'value'      => '',
        ),

        /* Phone No. */
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Contact Title', 'digicrew' ),
            'param_name' => 'phone_title',
            'description'=> esc_html__( 'Enter Contact Title.', 'digicrew' ),
            'value'      => esc_html__( 'PHONE', 'digicrew' ),
        ),
         array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Contact Number', 'digicrew' ),
            'param_name' => 'phone_no',
            'description'=> esc_html__( 'Enter Contact Number.','digicrew' ),
            'value'      => '',
        ),        


        /* Address*/
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Address Title', 'digicrew' ),
            'param_name' => 'address_title',
            'description'=> esc_html__( 'Enter Address Title.', 'digicrew' ),
            'value'      => esc_html__( 'ADDRESS', 'digicrew' ),
            
        ),

        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Address Text', 'digicrew' ),
            'param_name' => 'address_text',
            'description'=> esc_html__( 'Enter Address Text.', 'digicrew' ),
            'value'      => '',
            
        ),
       
        /* Extra */
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name' => 'el_class',
            'description'=> esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
        ),
    )
));

class WPBakeryShortCode_digicrew_address_box extends DSShortCode
{
    protected function content($atts, $content = null)
    {
        return parent::content($atts, $content);
    }
}