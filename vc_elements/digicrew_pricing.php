<?php
vc_map(array(
    'name'        => esc_html__( 'Pricing', 'digicrew' ),
    'base'        => 'digicrew_pricing',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Pricing Displayed','digicrew' ),
    'category'    => esc_html__( 'Digicrew Shortcodes', 'digicrew' ),
    'params'      => array(
        
        /* Layout Classic */
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Title', 'digicrew' ),
            'param_name' => 'title',
            'value'      => '',
            'group'      => esc_html__( 'Source Settings', 'digicrew' ),
        ),
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Currency', 'digicrew' ),
            'param_name' => 'currency',
            'value'      => '',
            'group'      => esc_html__( 'Source Settings', 'digicrew' ),
        ),
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Price', 'digicrew' ),
            'param_name' => 'price',
            'value'      => '',
            'group'      => esc_html__( 'Source Settings', 'digicrew' ),
        ),
        
        array(
            'type'       => 'param_group',
            'heading'    => esc_html__( 'Description', 'digicrew' ),
            'param_name' => 'description',
            'value'      => '',
            'group'      => esc_html__( 'Source Settings', 'digicrew' ),
            'params'     => array(
                array(
                    "type"       => "textfield",
                    "heading"    => esc_html__( "Item", 'digicrew' ),
                    "param_name" => "description_item",
                    'admin_label'=> true,
                ),
            ),
        ),
        array(
            'type'       => 'checkbox',
            'heading'    => esc_html__( "Show Active", 'digicrew' ),
            'param_name' => 'show_active',
            'value'      => array( 'Yes' => true ),
            'description'=> esc_html__( "Check if you want to Active the Plan please check.", 'digicrew' ),
            'group'      => esc_html__( 'Source Settings', 'digicrew' ),
        ),
        array(
            'type'             => 'textfield',
            'heading'          => esc_html__( 'Text Button', 'digicrew' ),
            'param_name'       => 'text_button',
            'value'            => '',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'group'            => esc_html__( 'Source Settings', 'digicrew' ),
        ),
        array(
            'type'             => 'vc_link',
            'heading'          => esc_html__( 'Link Button', 'digicrew' ),
            'param_name'       => 'link_button',
            'value'            => '',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'group'            => esc_html__( 'Source Settings', 'digicrew' ),
        ),
        /* Extra */
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name'  => 'el_class',
            'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
            'group'       => esc_html__( 'Extra', 'digicrew' )
        ),
        
    )
));

class WPBakeryShortCode_digicrew_pricing extends DSShortCode
{
    protected function content($atts, $content = null)
    {
        return parent::content($atts, $content);
    }
}