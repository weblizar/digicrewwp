<?php
vc_map(array(
    'name'        => esc_html__( 'Button' ,'digicrew' ),
    'base'        => 'digicrew_button',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Button Displayed', 'digicrew' ),
    'category'    => esc_html__( 'Digicrew Shortcodes', 'digicrew' ),
    'params'      => array(
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Text', 'digicrew' ),
            'param_name' => 'button_text',
            'value'      => '',
            'admin_label'=> true,
            'group'      => esc_html__( 'Button Settings', 'digicrew' )
        ),
        array(
            'type'             => 'vc_link',
            'class'            => '',
            'heading'          => esc_html__( 'Link', 'digicrew' ),
            'param_name'       => 'button_link',
            'value'            => '',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'group'            => esc_html__( 'Button Settings', 'digicrew' )
        ),
       
        
        array(
            'type'       => 'dropdown',
            'heading'    => esc_html__( 'Alignment', 'digicrew' ),
            'param_name' => 'align_lg',
            'value'      => array(
                'Left'   => 'left',
                'Center' => 'center',
                'Right'  => 'right',
            ),
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'group'            => esc_html__( 'Button Settings', 'digicrew' )
        ),
        array(
            'type'             => 'colorpicker',
            'heading'          => esc_html__( 'Text Color', 'digicrew' ),
            'param_name'       => 'text_color',
            'value'            => '',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'group'            => esc_html__( 'Button Settings', 'digicrew' )
        ),
        array(
            'type'             => 'colorpicker',
            'heading'          => esc_html__( 'BG Color', 'digicrew' ),
            'param_name'       => 'bg_color',
            'value'            => '',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'group'            => esc_html__( 'Button Settings', 'digicrew' )
        ),
        array(
            'type'             => 'textfield',
            'heading'          => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name'       => 'el_class',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'description'      => esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
            'group'            => esc_html__( 'Button Settings', 'digicrew' )
        ),
        array(
            'type'             => 'checkbox',
            'heading'          => esc_html__( "Add Second Button?", 'digicrew' ),
            'param_name'       => 'add_button',
            'value'            => array(  'Yes'  => true ),
            'description'      => esc_html__( "Check if you want to add another button.", 'digicrew' ),
            'group'            => esc_html__( 'Button Settings', 'digicrew' )
        ),
        array(
            'type'          => 'textfield',
            'heading'       => esc_html__( 'Text', 'digicrew' ),
            'param_name'    => 'button_text2',
            'value'         => '',
            'admin_label'   => true,
            'group'         => esc_html__( 'Button 2 Settings', 'digicrew' ),
            'dependency'    => array( 
                'element' => 'add_button', 
                'not_empty' => true
            )
        ),
        array(
            'type'             => 'vc_link',
            'class'            => '',
            'heading'          => esc_html__( 'Link', 'digicrew' ),
            'param_name'       => 'button_link2',
            'value'            => '',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'group'            => esc_html__( 'Button 2 Settings', 'digicrew' ),
            'dependency'       => array( 
                'element' => 'add_button', 
                'not_empty' => true 
            )
        ),
        
        array(
            'type'             => 'colorpicker',
            'heading'          => esc_html__( 'Text Color', 'digicrew' ),
            'param_name'       => 'text_color2',
            'value'            => '',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'group'            => esc_html__( 'Button 2 Settings', 'digicrew' ),
            'dependency'       => array( 
                'element' => 'add_button', 
                'not_empty' => true 
            )
        ),
        array(
            'type'             => 'colorpicker',
            'heading'          => esc_html__( 'BG Color', 'digicrew' ),
            'param_name'       => 'bg_color2',
            'value'            => '',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'group'            => esc_html__( 'Button 2 Settings', 'digicrew' ),
            'dependency'       => array( 
                'element' => 'add_button', 
                'not_empty' => true 
            )
        ),
        array(
            'type'             => 'textfield',
            'heading'          => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name'       => 'el_class2',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'description'      => esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
            'group'            => esc_html__( 'Button 2 Settings', 'digicrew' ),
            'dependency'       => array( 
                'element' => 'add_button', 
                'not_empty' => true 
            )
        ),
    )
));

class WPBakeryShortCode_digicrew_button extends DSShortCode
{
    protected function content($atts, $content = null)
    {
        return parent::content($atts, $content);
    }
}