<?php
vc_map( array(
    'name'        => esc_html__( 'Counter', 'digicrew' ),
    'base'        => 'digicrew_counter',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Counter Displayed', 'digicrew' ),
    'category'    => esc_html__( 'Digicrew Shortcodes', 'digicrew' ),
    'params'      => array(

       
        /* Title */
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Title', 'digicrew' ),
            'param_name'  => 'title',
            'description' => esc_html__( 'Enter title.', 'digicrew' ),
            'admin_label' => true,
        ),
        
        /* Digit */
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Digit', 'digicrew' ),
            'param_name'  => 'digit',
            'description' => esc_html__( 'Enter digit.', 'digicrew' ),
        ),
        'admin_label'     => true,

        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => esc_html__( "Color", 'digicrew' ),
            "param_name"  => "color",
            "value"       => array (
                "Yellow"  => "yellow", 
                "Red"     => "red", 
                "Sky"     => "sky", 
            ),
            'admin_label' => true,
        ),
        
        /* Extra */
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name' => 'el_class',
            'description'=> esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
        ),

    )
));

class WPBakeryShortCode_digicrew_counter extends DSShortCode
{
    protected function content($atts, $content = null)
    {
        return parent::content($atts, $content);
    }
}