<?php
vc_map(
    array(
    'name'        => esc_html__('Banner','digicrew' ),
    'base'        => 'digicrew_slider',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Banner Image Displayed', 'digicrew' ),
    'category'    => esc_html__('Digicrew Shortcodes', 'digicrew'),
    'params'      => array(

        /* Heading 1 */
        array(
            'type'          => 'textfield',
            'heading'       => esc_html__('Heading 1', 'digicrew'),
            'param_name'    => 'heading',
            'admin_label'   => true,
        ),
        /* Heading 2*/
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__('Heading 2', 'digicrew'),
            'param_name'  => 'heading_2',
            'admin_label' => true,
        ),

        /* Description */
        array(
            'type'       => 'textarea',
            'heading'    => esc_html__('Description', 'digicrew'),
            'param_name' => 'sub_heading',
        ),

        /* Image*/
        array(
            'type'        => 'attach_image',
            'heading'     => esc_html__( 'Background Image', 'digicrew' ),
            'param_name'  => 'bg_image',
            'value'       => '',
            'description' => esc_html__( 'Select image from media library.', 'digicrew' ),
        ),

        /* Image*/
        array(
            'type'        => 'attach_image',
            'heading'     => esc_html__( 'Right Side Image', 'digicrew' ),
            'param_name'  => 'ryt_image',
            'value'       => '',
            'description' => esc_html__( 'Select image from media library.', 'digicrew' ),
        ),

        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Button 1 Text ', 'digicrew' ),
            'param_name' => 'btn_txt_1',
            'value'      => '',
        ),

        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Button 1 URL', 'digicrew' ),
            'param_name' => 'btn_url_1',
            'value'      => '',
        ),

        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Button 2 Text ', 'digicrew' ),
            'param_name' => 'btn_txt_2',
            'value'      => '',
        ),

        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Button 2 URL', 'digicrew' ),
            'param_name' => 'btn_url_2',
            'value'      => '',
        ),

        /* Extra */
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name'  => 'el_class',
            'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
        ),
    )
));

class WPBakeryShortCode_digicrew_slider extends DSShortCode
{

    protected function content($atts, $content = null)
    {
        return parent::content($atts, $content);
    }
}