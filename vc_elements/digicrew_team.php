<?php
vc_map(array(
    'name'        => esc_html__('Team','digicrew' ),
    'base'        => 'digicrew_team',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Team Displayed', 'digicrew' ),
    'category'    => esc_html__('Digicrew Shortcodes', 'digicrew'),
    'params'      => array(

        /* Title */
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__('Title', 'digicrew'),
            'param_name' => 'title',
            'admin_label'=> true,
        ),
        
        /* Position */
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__('Position', 'digicrew'),
            'param_name' => 'position',

        ),

        /* Image*/
        array(
            'type'        => 'attach_image',
            'heading'     => esc_html__( 'Image', 'digicrew' ),
            'param_name'  => 'image',
            'value'       => '',
            'description' => esc_html__( 'Select image from media library.', 'digicrew' ),
        ),

        array(
            'type'       => 'textfield',
            'heading'    => esc_html__('Facebook URL', 'digicrew'),
            'param_name' => 'facebook_url',
            'value'      => '',
        ),

        array(
            'type'       => 'textfield',
            'heading'    => esc_html__('Twitter URL', 'digicrew'),
            'param_name' => 'twitter_url',
            'value'      => '',
        ),

        array(
            'type'       => 'textfield',
            'heading'    => esc_html__('Instagram URL', 'digicrew'),
            'param_name' => 'instagram_url',
            'value'      => '',
        ),

        array(
            'type'       => 'textfield',
            'heading'    => esc_html__('Linkedin URL', 'digicrew'),
            'param_name' => 'linkedin_url',
            'value'      => '',
        ),
       
        /* Extra */
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name'  => 'el_class',
            'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
        ),
    )
));

class WPBakeryShortCode_digicrew_team extends DSShortCode
{
    protected function content($atts, $content = null)
    {
        return parent::content($atts, $content);
    }
}