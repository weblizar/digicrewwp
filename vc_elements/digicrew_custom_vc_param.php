<?php
/*
 * VC
 */
vc_add_param("vc_column", array(
    "type"                      => "dropdown",
    "class"                     => "",
    "heading"                   => esc_html__( "Custom Style", 'digicrew' ),
    "param_name"                => "ct_column_class",
    "value"                     => array(
        'None'                  => '',
        'Video Background Image'=> 'fixed-bg',
        'SEO Analysis Form'     => 'seo-analysis-form',
	),
    "group"                     => esc_html__( "Customs", 'digicrew' ),
));