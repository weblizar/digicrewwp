<?php
$args = array(
    'name'        => esc_html__( 'Feature Slider', 'digicrew' ),
    'base'        => 'digicrew_feature_slider',
    'class'       => 'digicrew-icon-element',
    'description' => esc_html__( 'Feature Displayed', 'digicrew' ),
    'category'    => esc_html__( 'Digicrew Shortcodes', 'digicrew' ),
    'params'      => array(

        array(
            'type'       => 'param_group',
            'heading'    => esc_html__( 'Feature Item', 'digicrew' ),
            'value'      => '',
            'param_name' => 'feature_item',
            'params'     => array(
                array(
                    'type'       => 'textfield',
                    'heading'    => esc_html__( 'Title', 'digicrew' ),
                    'param_name' => 'title',
                    'admin_label'=> true,
                ),
                /* Icon */
                array(
                    'type'       => 'dropdown',
                    'heading'    => esc_html__( 'Icon Library', 'digicrew' ),
                    'value'      => array(
                        esc_html__( 'Font Awesome', 'digicrew' ) => 'fontawesome',
                        esc_html__( 'Flaticon', 'digicrew' )     => 'flaticon',
                    ),
                    'param_name' => 'icon_list',
                    'description'=> esc_html__( 'Select icon library.', 'digicrew' ),
                ),
                array(
                    'type'       => 'iconpicker',
                    'heading'    => esc_html__( 'Icon FontAwesome', 'digicrew' ),
                    'param_name' => 'icon_fontawesome',
                    'value'      => '',
                    'settings'   => array(
                        'emptyIcon'    => true,
                        'type'         => 'fontawesome',
                        'iconsPerPage' => 200,
                    ),
                    'dependency'  => array(
                        'element' => 'icon_list',
                        'value'   => 'fontawesome',
                    ),
                    'description' => esc_html__( 'Select icon from library.', 'digicrew' ),
                ),  
                array(
                    'type'             => 'iconpicker',
                    'heading'          => esc_html__( 'Flaticon', 'digicrew' ),
                    'param_name'       => 'icon_flaticon',
                    'settings'         => array(
                        'emptyIcon'    => true,
                        'type'         => 'flaticon',
                        'iconsPerPage' => 200,
                    ),
                    'dependency'  => array(
                        'element' => 'icon_list',
                        'value'   => 'flaticon',
                    ),
                    'default'     => 'flaticon-system',
                    'description' => esc_html__( 'Select icon from library.', 'digicrew' ),
                ),
            ),

        ),
        
        array(
            'type'       => 'textfield',
            'heading'    => esc_html__( 'Extra class name', 'digicrew' ),
            'param_name' => 'el_class',
            'description'=> esc_html__( 'Style particular content element differently - add a class name and refer to it in Custom CSS.', 'digicrew' ),
        ),
    )
);

vc_map($args);  

class WPBakeryShortCode_digicrew_feature_slider extends DSShortCode {
    protected function content($atts, $content = null) {
        return parent::content($atts, $content);
    }
}