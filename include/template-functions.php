<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Digicrew
 */

defined( 'ABSPATH' ) or die();

/* Get the plugin */
if ( ! function_exists( 'digicrewaddons_active' ) ) {
    function digicrewaddons_active() {
        if ( function_exists( 'DigicrewAddons' ) ) {
            return true;
        } else {
            return false;
        }
    }
}
#-----------------------------------------------------------------#
# plugin activation 
#-----------------------------------------------------------------# 
require_once get_template_directory() . '/include/libs/class-tgm-plugin-activation.php';
require_once get_template_directory() . '/include/require-plugins.php'; 

function digicrew_vc_elements() {
    if (class_exists('DSShortCode')) {
        cms_require_folder('vc_elements', get_template_directory() );
    }
}
add_action('vc_before_init', 'digicrew_vc_elements');


#-----------------------------------------------------------------#
# Change The Default WordPress Excerpt Length
#-----------------------------------------------------------------#
if ( ! function_exists( 'digicrew_custom_excerpt' ) ) {
    function digicrew_custom_excerpt($limit) {

        $excerpt = explode(' ', get_the_excerpt(), $limit);
    
        if ( count($excerpt) >= $limit ) {
            array_pop($excerpt);
            $excerpt = implode(" ", $excerpt) . '...';
        } else {
            $excerpt = implode(" ", $excerpt);
        }
        $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    
        return $excerpt;
    }
}

/**
 * Comment Form
*/
function digicrew_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}
add_filter( 'comment_form_fields', 'digicrew_comment_field_to_bottom' );

function digicrew_excerpt_global_length( $length ) {
  return 20;
}

// Theme Except Functions 
function digicrew_excerpt(){
  add_filter( 'excerpt_length', 'digicrew_excerpt_global_length', 999 );
  echo get_the_excerpt();
}


/**
 * Prints posts pagination based on query
 *
 * @param  WP_Query $query Custom query, if left blank, this will use global query ( current query )
 * @return void
 */
function digicrew_theme_pagination( $query = null ) {
    $classes = array();

    if ( empty( $query ) )
    {
        $query = $GLOBALS['wp_query'];
    }

    if ( empty( $query->max_num_pages ) || ! is_numeric( $query->max_num_pages ) || $query->max_num_pages < 2 )
    {
        return;
    }

    $paged = get_query_var( 'paged' );

    if ( ! $paged && is_front_page() && ! is_home() )
    {
        $paged = get_query_var( 'page' );
    }

    $paged = $paged ? intval( $paged ) : 1;

    $pagenum_link = html_entity_decode( get_pagenum_link() );
    $query_args   = array();
    $url_parts    = explode( '?', $pagenum_link );

    if ( isset( $url_parts[1] ) )
    {
        wp_parse_str( $url_parts[1], $query_args );
    }

    $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
    $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

    $html_prev = '<i class="fas fa-arrow-left"></i>';
    $html_next = '<i class="fas fa-arrow-right"></i>';
    $links = paginate_links( array(
        'base'     => $pagenum_link,
        'total'    => $query->max_num_pages,
        'current'  => $paged,
        'type'     => 'plain',
        'add_args' => array_map( 'urlencode', $query_args ),
        'prev_text' => $html_prev,
        'next_text' => $html_next,
    ) );

    $template = '<div class="pagination">%2$s</div>';

    if ( $links )
    {
        printf(
            wp_kses(
                $template, 
                array(
                    'div' => array(
                    'class' => array(),
                    )
                )
            ),
            esc_html__( 'Navigation', 'digicrew' ),
            wp_kses(
                $links, 
                array(
                    'div' => array(
                    'class' => array(),
                    ),
                    'a' => array(
                        'class' => array(),
                        'href'  => array(),
                        'title' => array()
                    ),
                    'span' => array(
                        'class' => array(),
                    ),
                    'i' => array(
                        'class' => array(),
                    ),
                )
            )
        );
    }
}

/**
 * Custom Comment List
*/
function digicrew_comment_list( $comment, $args, $depth ) {
    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <<?php echo ''.$tag ?> <?php comment_class( empty( $args['has_children'] ) ? 'comment-item ' : 'comment-item parent' ) ?> id="comment-<?php comment_ID() ?>">
    
    <div class="left-comment-box">
        <?php if ($args['avatar_size'] != 0) echo get_avatar($comment, $args['avatar_size']); ?>
    </div>
    <div class="right-comment-content">
        <h3 class="comment-title"><?php printf('%s', get_comment_author_link()); ?> |<span><?php echo get_comment_date() . ' at ' . get_comment_time(); ?></span></h3>
        <?php comment_reply_link(array_merge($args, array('reply_text' => __('<i class="fas fa-reply-all"></i>', 'digicrew'),'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
        <?php if ( $comment->comment_approved == '0' ) : ?>
        <p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'digicrew' ); ?></p>
        
        <?php endif; ?> 
        <?php comment_text(); ?>

    </div>
    
<?php 
}

/**
 * Page title layout
 **/
function digicrew_page_title_layout() {
    get_template_part( 'template-parts/entry-header', '' );
}

/**
 * Get page title and description.
 *
 * @return array Contains 'title'
 */
function digicrew_get_page_titles() {
    $title = '';

    // Default titles
    if ( ! is_archive() ) {
        // Posts page view
        if ( is_home() ) {
            // Only available if posts page is set.
            if ( ! is_front_page() && $page_for_posts = get_option( 'page_for_posts' ) ) {
                $title = get_post_meta( $page_for_posts, 'custom_title', true );
                if ( empty( $title ) ) {
                    $title = get_the_title( $page_for_posts );
                }
            }
            if ( is_front_page() ) {
                $title = esc_html__( 'Blog', 'digicrew' );
            }
        } // Single page view
        elseif ( is_page() ) {
            $title = get_post_meta( get_the_ID(), 'custom_title', true );
            if ( ! $title ) {
                $title = get_the_title();
            }
        } elseif ( is_404() ) {
            $title = esc_html__( 'Page Not Found', 'digicrew' );
        } elseif ( is_search() ) {
            $title = esc_html__( 'Search results for:', 'digicrew' ). ' ' . get_search_query();
        } // Single page view
        elseif ( is_singular( 'portfolio') ) {
            $title = get_post_meta( get_the_ID(), 'custom_title', true );
            if ( ! $title ) {
                $title = get_the_title(); 
            }
        } else {
            $title = get_post_meta( get_the_ID(), 'custom_title', true );
            if ( ! $title ) {
                $title = get_the_title();
            }
        }
    } elseif ( is_author() ) {
        $title = esc_html__( 'Author:', 'digicrew' ) . ' ' . get_the_author();
    } // Author
    else {
        $title = get_the_archive_title();
    }

    return array(
        'title' => $title,
    );
}

/***************** Create the breadcrumbs ******************/

if (!function_exists('digicrew_breadcrumbs')) {
    
    function digicrew_breadcrumbs() {

        $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show

        $home = esc_html__('Home', 'digicrew'); // text for the 'Home' link

        $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show

        global $post;

        $homeLink = home_url();

        if (is_home() || is_front_page()) {
            if ($showOnHome == 1) echo '<li class="breadcrumb-item"><a href="'.esc_url($homeLink).'">'.esc_html($home).'</a></li>';
        }
        else {
            echo '<li class="breadcrumb-item"><a href="'.esc_url($homeLink).'">'.esc_html($home).'</a></li>';
            //echo '<li class="breadcrumb-item active">';

            if ( is_category() ) {
              
                global $wp_query;

                $cat_obj = $wp_query->get_queried_object();

                $thisCat = $cat_obj->term_id;

                $thisCat = get_category($thisCat);

                $parentCat = get_category($thisCat->parent);

                if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' '));

                echo '<li class="breadcrumb-item active">'. esc_html__('Archive by category', 'digicrew').' "' . single_cat_title('', false) . '"'."</li>";

            }
            elseif ( is_search() ) {
         
                echo '<li class="breadcrumb-item active">'.esc_html__('Results for', 'digicrew').' "' . get_search_query() . '"'."</li>";
            }
            elseif ( is_day() ) {
              
                echo '<li class="breadcrumb-item"><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ';

                echo '<li class="breadcrumb-item"><a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a></li> ';

                echo '<li class="breadcrumb-item active">'.get_the_time('d')."</li>";
            }
            elseif ( is_month() ) {
             
                echo '<li class="breadcrumb-item"><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ';

                echo '<li class="breadcrumb-item active">'.get_the_time('F')."</li>";

            }
            elseif ( is_year() ) {
            
                echo '<li class="breadcrumb-item active">'.get_the_time('Y')."</li>";

            }
            elseif ( is_single() && !is_attachment() ) {
            
                if ( get_post_type() != 'post' ) {

                    $post_type = get_post_type_object(get_post_type());

                    $slug = $post_type->rewrite;

                    echo '<li class="breadcrumb-item"><a href="' . esc_url($homeLink) . '/' . $slug['slug'] . '/">' . $slug['slug'] . '</a></li>';

                    if ($showCurrent == 1) echo '<li class="breadcrumb-item active">'.get_the_title().'</li>';
                }
                else {
             
                    $cat = get_the_category(); $cat = $cat[0];

                    is_wp_error( $cats = get_category_parents($cat, TRUE, ' ') ) ? '' : $cats; 

                    if ($showCurrent == 0) $cats = preg_replace("/^(.+)\s \s$/", "$1", $cats);

                    if ($showCurrent == 1) echo '<li class="breadcrumb-item active">'.get_the_title().'</li>';
                }
            }
            elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {

                $post_type = get_post_type_object(get_post_type());

                echo get_post_type_object(get_post_type())->labels->singular_name;
            }
            elseif ( is_attachment() ) {
                    
                $parent = get_post($post->post_parent);

                $cat = get_the_category($parent->ID); $cat = $cat[0];

                if ($cat || !is_wp_error($cat) ){
                    echo get_category_parents($cat, TRUE, ' ');
                }

                echo get_category_parents($cat, TRUE, ' ');

                echo '<li class="breadcrumb-item"><a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a></li>';

                if ($showCurrent == 1) echo '<li class="breadcrumb-item active">'.get_the_title().'</li>';
            }
            elseif ( is_page() && !$post->post_parent ) {

                if ($showCurrent == 1) echo '<li class="breadcrumb-item active">'.get_the_title().'</li>';
            }
            elseif ( is_page() && $post->post_parent ) {

                $parent_id  = $post->post_parent;

                $breadcrumbs = array();

                while ($parent_id) {

                    $page = get_page($parent_id);

                    $breadcrumbs[] = '<li class="breadcrumb-item"><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li> ';

                    $parent_id  = $page->post_parent;
                }

                $breadcrumbs = array_reverse($breadcrumbs);

                foreach ($breadcrumbs as $crumb) echo !empty( $crumb ) ? $crumb : '';

                if ($showCurrent == 1) echo '<li class="breadcrumb-item active">'.get_the_title().'</li>';

            }
            elseif ( is_tag() ) {

                echo '<li class="breadcrumb-item active">'.esc_html__('Posts tagged for ', 'digicrew').' "' . single_tag_title('', false) . '"'.'</li>';

            }
            elseif ( is_author() ) {

                global $author;

                $userdata = get_userdata($author);

                echo '<li class="breadcrumb-item active">'.esc_html__('Articles posted by', 'digicrew').' ' . $userdata->display_name.'</li>';

            }
            elseif ( is_404() ) {

                echo '<li class="breadcrumb-item active">'.esc_html__('Error 404', 'digicrew').'</li>';

            }

            if ( get_query_var('paged') ) {

                

                echo '<li class="breadcrumb-item active">'.esc_html__('Page', 'digicrew') . ' ' . get_query_var('paged').'</li>';

                
            }

            //echo '</li>';
        }
    }
}


//  Function From Ref Theme

/**
 * Get opt_name for Redux Framework options instance args and for
 * getting option value.
 *
 * @return string
 */
function digicrew_get_opt_name() {
    return apply_filters( 'digicrew_opt_name', 'cms_theme_options' );
}

/**
 * Get theme option based on its id.
 *
 * @param  string $opt_id Required. the option id.
 * @param  mixed $default Optional. Default if the option is not found or not yet saved.
 *                         If not set, false will be used
 *
 * @return mixed
 */
function digicrew_get_opt( $opt_id, $default = false ) {
    $opt_name = digicrew_get_opt_name();
    if ( empty( $opt_name ) ) {
        return $default;
    }

    global ${$opt_name};
    if ( ! isset( ${$opt_name} ) || ! isset( ${$opt_name}[ $opt_id ] ) ) {
        $options = get_option( $opt_name );
    } else {
        $options = ${$opt_name};
    }
    if ( ! isset( $options ) || ! isset( $options[ $opt_id ] ) || $options[ $opt_id ] === '' ) {
        return $default;
    }
    if ( is_array( $options[ $opt_id ] ) && is_array( $default ) ) {
        foreach ( $options[ $opt_id ] as $key => $value ) {
            if ( isset( $default[ $key ] ) && $value === '' ) {
                $options[ $opt_id ][ $key ] = $default[ $key ];
            }
        }
    }

    return $options[ $opt_id ];
}

/**
 * Get opt_name for Redux Framework options instance args and for
 * getting option value.
 *
 * @return string
 */
function digicrew_get_page_opt_name() {
    return apply_filters( 'digicrew_page_opt_name', 'cms_page_options' );
}


/**
 * Check if provided color string is valid color.
 * Only supports 'transparent', HEX, RGB, RGBA.
 *
 * @param  string $color
 *
 * @return boolean
 */
function digicrew_is_valid_color( $color ) {
    $color = preg_replace( "/\s+/m", '', $color );

    if ( $color === 'transparent' ) {
        return true;
    }

    if ( '' == $color ) {
        return false;
    }

    // Hex format
    if ( preg_match( "/(?:^#[a-fA-F0-9]{6}$)|(?:^#[a-fA-F0-9]{3}$)/", $color ) ) {
        return true;
    }

    // rgb or rgba format
    if ( preg_match( "/(?:^rgba\(\d+\,\d+\,\d+\,(?:\d*(?:\.\d+)?)\)$)|(?:^rgb\(\d+\,\d+\,\d+\)$)/", $color ) ) {
        preg_match_all( "/\d+\.*\d*/", $color, $matches );
        if ( empty( $matches ) || empty( $matches[0] ) ) {
            return false;
        }

        $red   = empty( $matches[0][0] ) ? $matches[0][0] : 0;
        $green = empty( $matches[0][1] ) ? $matches[0][1] : 0;
        $blue  = empty( $matches[0][2] ) ? $matches[0][2] : 0;
        $alpha = empty( $matches[0][3] ) ? $matches[0][3] : 1;

        if ( $red < 0 || $red > 255 || $green < 0 || $green > 255 || $blue < 0 || $blue > 255 || $alpha < 0 || $alpha > 1.0 ) {
            return false;
        }
    } else {
        return false;
    }

    return true;
}

/**
 * Get theme option based on its id.
 *
 * @param  string $opt_id Required. the option id.
 * @param  mixed $default Optional. Default if the option is not found or not yet saved.
 *                         If not set, false will be used
 *
 * @return mixed
 */
function digicrew_get_page_opt( $opt_id, $default = false ) {
    $page_opt_name = digicrew_get_page_opt_name();
    if ( empty( $page_opt_name ) ) {
        return $default;
    }
    $id = get_the_ID();
    if ( ! is_archive() && is_home() ) {
        if ( ! is_front_page() ) {
            $page_for_posts = get_option( 'page_for_posts' );
            $id             = $page_for_posts;
        }
    }

    return $options = ! empty($id) ? get_post_meta( intval( $id ), $opt_id, true ) : $default;
}

function digicrew_vc_shortcode_css_class( $classes, $settings_base, $atts ) {
    $classes_arr = explode( ' ', $classes );

    if ( 'vc_row' == $settings_base ) {
        if (isset($atts['digicrew_row_class'])  ) {
            $classes_arr[] = $atts['digicrew_row_class'];
        }
        if ( isset($atts['bg_image_position']) ) {
            $classes_arr[] = $atts['bg_image_position'];
        }
    }

    
    if ( 'vc_row_inner' == $settings_base ) {
        if ( isset($atts['row_border_box']) ) {
            $classes_arr[] = $atts['row_border_box'];
        }
    }

    if ( isset($atts['animation_column']) && $atts['animation_column'] ) {
        wp_enqueue_style( 'animate-css' );
        $classes_arr[] = 'wpb_animate_when_almost_visible '.' wpb_'.$atts['animation_column'].' '.$atts['animation_column'];
    }

    if ( 'vc_column_inner' == $settings_base ) {
        if ( isset($atts['ct_column_inner_class']) ) {
            $classes_arr[] = $atts['ct_column_inner_class'];
        }
    }
    if ( 'vc_column' == $settings_base ) {
        if ( isset($atts['ct_column_class']) ) {
            $classes_arr[] = $atts['ct_column_class'];
        }
    }

    if ( 'vc_single_image' == $settings_base ) {
        if ( isset($atts['ct_image_align']) ) {
            $classes_arr[] = $atts['ct_image_align'];
        }
        if ( isset($atts['ct_image_align_md']) ) {
            $classes_arr[] = $atts['ct_image_align_md'];
        }
    }

    if ( 'vc_column_text' == $settings_base ) {
        if ( isset($atts['text_align'] )) {
            $classes_arr[] = $atts['text_align'];
        }
    }

    return implode( ' ', $classes_arr );
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    add_filter( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'digicrew_vc_shortcode_css_class', 10, 3 );
}

/**
 * Minify css
 *
 * @param  string $css
 *
 * @return string
 */
function digicrew_css_minifier( $css ) {
    // Normalize whitespace
    $css = preg_replace( '/\s+/', ' ', $css );
    // Remove spaces before and after comment
    $css = preg_replace( '/(\s+)(\/\*(.*?)\*\/)(\s+)/', '$2', $css );
    // Remove comment blocks, everything between /* and */, unless
    // preserved with /*! ... */ or /** ... */
    $css = preg_replace( '~/\*(?![\!|\*])(.*?)\*/~', '', $css );
    // Remove ; before }
    $css = preg_replace( '/;(?=\s*})/', '', $css );
    // Remove space after , : ; { } */ >
    $css = preg_replace( '/(,|:|;|\{|}|\*\/|>) /', '$1', $css );
    // Remove space before , ; { } ( ) >
    $css = preg_replace( '/ (,|;|\{|}|\(|\)|>)/', '$1', $css );
    // Strips leading 0 on decimal values (converts 0.5px into .5px)
    $css = preg_replace( '/(:| )0\.([0-9]+)(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}.${2}${3}', $css );
    // Strips units if value is 0 (converts 0px to 0)
    $css = preg_replace( '/(:| )(\.?)0(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}0', $css );
    // Converts all zeros value into short-hand
    $css = preg_replace( '/0 0 0 0/', '0', $css );
    // Shortern 6-character hex color codes to 3-character where possible
    $css = preg_replace( '/#([a-f0-9])\\1([a-f0-9])\\2([a-f0-9])\\3/i', '#\1\2\3', $css );

    return trim( $css );
}

/**
 * Header layout
**/
function digicrew_header_layout() {
    get_template_part( 'template-parts/header' );
}


/**
 * Header Tracking Code to wp_head hook.
 */
function digicrew_header_code() {
    $site_header_code = digicrew_get_opt( 'site_header_code' );
    if ( $site_header_code !== '' ) {
        print wp_kses( $site_header_code, wp_kses_allowed_html() );
    }
}

add_action( 'wp_head', 'digicrew_header_code' );


/**
 * Footer Tracking Code to wp_footer hook.
 */
function digicrew_footer_code() {
    $site_footer_code = digicrew_get_opt( 'site_footer_code' );
    if ( $site_footer_code !== '' ) {
        print wp_kses( $site_footer_code, wp_kses_allowed_html() );
    }
}

add_action( 'wp_footer', 'digicrew_footer_code' );

if ( ! function_exists( 'digicrew_customize_search_form' ) ) :

    /** Customize search form.
     **/
    function digicrew_customize_search_form() {

        $form = '<div class="search-box"><form method="post" id="searchform" action="' . esc_url( home_url( '/' ) ) . '">
          <input type="text" class="form-control" placeholder="' . esc_attr_x( 'Enter your keywords...', 'placeholder', 'digicrew' ) . '" value="' . get_search_query() . '" name="s" />
                <button type="submit" class="fa fa-search btn-search"></button>
            </form></div>';
        return $form;
    }
    
endif;

add_filter( 'get_search_form', 'digicrew_customize_search_form', 15 );

#Get all portfolio category inline

function digicrew_showPortCategory($port_cats = "")
{
    if (!isset($term_list)) {
        $term_list = '';
    }
    $permalink = get_permalink();
    $args = array('taxonomy' => 'catportfolio', 'hide_empty' => true);
    $terms = get_terms('catportfolio', $args);
    $count = count($terms);
    $i = 0;
    $iterm = 1;
    if ($count > 0) {
        if (!isset($_GET['slug'])) $all_current = 'active';
        $cape_list = '';
        $term_list .= '<button class="'.$all_current.'" data-filter="*">'. esc_html__('All', 'digicrew') .'</button>';
        $termcount = count($terms);
 
        if($port_cats[0] !== 'all' && $port_cats[0] !== '') {
            foreach ( $port_cats as $port_cat ) {
                $term = get_term_by('slug', $port_cat, 'catportfolio' );
                $term_list .= '<button data-filter=".'.$term->slug .'">'.$term->name.'</button>';
            }
        }
        else {
            if (is_array($terms)) {
                foreach ($terms as $term) {
                    $i++;
                    $permalink = esc_url(add_query_arg("slug", $term->slug, $permalink));
                    $term_list .= '<button data-filter=".'.$term->slug.'" ';
                    if (isset($_GET['slug'])) {
                        $getslug = $_GET['slug'];
                    } else {
                        $getslug = '';
                    }
                    if (strnatcasecmp($getslug, $term->name) == 0) $term_list .= 'class=" active"';
                    $tempname = strtr($term->name, array(
                        ' ' => '-',
                    ));
                    $tempname = strtolower($tempname);
                    $term_list .= '>'. $term->name . '</button>';
                    if ($count != $i) $term_list .= ' '; else $term_list .= '';
                    if ($iterm<$termcount) {$term_list .= '';}
                    $iterm++;
                }
            }
        }
        echo '<div class="gallery-filter-btn isotop-filter">' . $term_list . '</div>';
    }
}

/**
 * Script to wp_footer hook.
*/
function digicrew_menu_assets() {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
        'use strict';
            jQuery('.slicknav_brand a').attr("href", "<?php echo esc_url( home_url('/') ); ?>");
        });
    </script>
    <?php
}
add_action( 'wp_footer', 'digicrew_menu_assets' );