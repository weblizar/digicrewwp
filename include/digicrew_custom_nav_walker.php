<?php
/**
 * Custom Walker
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
class digicrewCustomNavWalker extends Walker_Nav_Menu {

    public $megaMenuID;
    public $count;
    private $el;

    public function __construct()
    {
        $this->megaMenuID = 0;
        $this->count = 0;
    }

    function start_lvl( &$output, $depth = 0, $args = array() ){ //ul

        $indent = str_repeat("\t",$depth);

        if ($this->megaMenuID != 0) {
            $output .= "<ul class=\"row dropdown\">
                            ";
        }
        else {
            $submenu = ($depth >= 1) ? 'sub-dropdown' : 'dropdown';
            $output .= "\n$indent<ul class=\"$submenu depth_$depth\">\n";
        }
    }

    public function end_lvl(&$output, $depth = 0, $args = array())
    {
        if ($this->megaMenuID != 0) {
            $output .= "</ul>";
        }
        else {
            $output .= "</ul>";
        }
    }

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {

        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
        $heading = '';
        $widget = '';
        $class_names = $value = '';
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        if ($this->megaMenuID != 0 && $this->megaMenuID === intval($item->menu_item_parent)) {
      
            $column_divider = array_search('column-divider', $classes);
            if ($column_divider !== false) {
                $output .= "";
            }
        } else {
            $this->megaMenuID = 0;
            
        }

        // managing divider: add divider class to an element to get a divider before it.
        $divider_class_position = array_search('divider', $classes);
        if ($divider_class_position !== false) {
            $output .= "<li class=\"divider\"></li>\n";
            unset($classes[$divider_class_position]);
        }
        if (array_search('megamenu', $classes) !== false) {
            $this->megaMenuID = $item->ID;
        }


        if ( has_nav_menu( 'primary' ) ) {
            $classes[] = ($args->walker->has_children) ? '' : '';
        }
        $classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
        
        if( $depth == 0 ) { $classes[] = 'nav-item'; }
        else { $classes[] = 'nav-item'; }
        
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );

        $class_names = ' class="'. esc_attr( $class_names ).' menu-item-'. $item->ID . ' "';

        $output .= $indent . '<li' . $value . $class_names .'>';

        $attributes  = !empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= !empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
        $attributes .= !empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
        $attributes .= !empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

        $prepend = '';
        $append = '';
        $description  = ! empty( $item->description ) ? '' : '';

        if($depth != 0)
        {
            $description = $append = $prepend = "";
        }

        if( !empty  ( $item->menu_icon )){
            $prepend .= '<span class="' . esc_attr( $item->menu_icon ) .' digicrew_menu_icon"></span>';
        }
        
        $item_output = '';

        if(isset($args->before)){
            $item_output = $args->before; 
        }
        if ( has_nav_menu( 'primary' ) ) {
            if ( $args->walker->has_children & $depth ==0 ) {
                $item_output .= '<a class="nav-link dropdown-toggle" '. $attributes .'>';
            }
            elseif( $args->walker->has_children & $depth >= 1) {
                $item_output .= '<a class="nav-link dropdown-toggle right" '. $attributes .'>';
            }
            elseif( $depth >= 1) {
                $item_output .= '<a class="nav-link" '. $attributes .'>';

            }
            else{
                $item_output .= '<a class="nav-link" '. $attributes .'>';
            }
        }
        else {
            $item_output .= '<a class="nav-link" '. $attributes .' >';
        }

        if(isset($args->link_before)) {
            $item_output .= $args->link_before.$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
        }

        if(isset($args->link_after)){
            $item_output .= $description.$args->link_after;
        }

        $item_output .= '</a>';

        if(isset($args->before)){
            $item_output .= $args->after;
        }
        
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args, $id );
    }
}