<?php
if (!function_exists('digicrew_font_flaticon')) :

    add_filter( 'vc_iconpicker-type-flaticon', 'digicrew_font_flaticon' );
    /**
    * awesome class.
    * 
    * @return string[]
    * @author FOX.
    */
    function digicrew_font_flaticon( $icons ) {
        $flaticon = array (
            array('flaticon-data'             => esc_html('flaticon-data') ),
            array('flaticon-vector'           => esc_html('flaticon-vector') ),
            array('flaticon-web-design'       => esc_html('flaticon-web-design') ),
            array('flaticon-web-design-1'     => esc_html('flaticon-web-design-1') ),
            array('flaticon-layout'           => esc_html('flaticon-layout') ),
            array('flaticon-research'         => esc_html('flaticon-research') ),
            array('flaticon-internet'         => esc_html('flaticon-internet') ),
            array('flaticon-backup'           => esc_html('flaticon-backup') ),
            array('flaticon-cloud'            => esc_html('flaticon-cloud') ),
            array('flaticon-share'            => esc_html('flaticon-share') ),
            array('flaticon-statistics'       => esc_html('flaticon-statistics') ),
            array('flaticon-computer-graphic' => esc_html('flaticon-computer-graphic') ),
            array('flaticon-research-1'       => esc_html('flaticon-research-1') ),
            array('flaticon-ux'               => esc_html('flaticon-ux') ),
            array('flaticon-product'          => esc_html('flaticon-product') ),
        );
        return array_merge( $icons, $flaticon );
    }
endif;