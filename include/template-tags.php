<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Digicrew
 */

/**
 * Set primary content class based on sidebar position
 * 
 * @param  string $sidebar_pos
 * @param  string $extra_class
 */
function digicrew_primary_class( $sidebar_pos, $extra_class = '' ) {
    if ( is_page() ) :
        $sidebar_load = 'sidebar';
    else :
        $sidebar_load = 'sidebar';
    endif;

    if ( is_active_sidebar( $sidebar_load ) ) {
        $class = array( trim( $extra_class ) );
        switch ( $sidebar_pos )
        {
            case 'left':
                $class[] = 'col-lg-8 order-lg-2';
                break;

            case 'right':
                $class[] = 'col-lg-8 col-md-12';
                break;

            default:
                $class[] = 'col-lg-12';
			break;
        }

        $class = implode( ' ', array_filter( $class ) );

        if ( $class ) {
            echo ' class="' . esc_attr($class) . '"';
        }
    } else {
        echo ' class="posts"'; 
    }
}

/**
 * Set secondary content class based on sidebar position
 * 
 * @param  string $sidebar_pos
 * @param  string $extra_class
 */
function digicrew_secondary_class( $sidebar_pos, $extra_class = '' )
{
    if (is_page()) :
        $sidebar_load = 'sidebar';
    else :
        $sidebar_load = 'sidebar';
    endif;

    if ( is_active_sidebar( $sidebar_load ) ) {
        $class = array(trim($extra_class));
        switch ($sidebar_pos) {
            case 'left':
                $class[] = 'col-lg-4 order-lg-1';
                break;

            case 'right':
                $class[] = 'col-lg-4 col-md-12';
                break;

			default:
				$class[] = 'col-lg-4 col-md-12';
				break;
        }

        $class = implode(' ', array_filter($class));

        if ($class) {
            echo ' class="' . esc_attr($class) . '"';
        }
    }	
}

/**
 * Prints archive meta on blog
 */
if ( ! function_exists( 'digicrew_archive_meta' ) ) :
    function digicrew_archive_meta() {
        $archive_author_on   = digicrew_get_opt( 'archive_author_on', true );
		$archive_date_on     = digicrew_get_opt( 'archive_date_on', true );
		$archive_tags_on = digicrew_get_opt( 'archive_tags_on', true );
        if( $archive_author_on || $archive_tags_on || $archive_date_on ) : ?>
			<ul class="blog-meta">
        		<?php if($archive_author_on) : ?>
                	<li><a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID')),'digicrew'); ?>"><i class="fas fa-user"></i><?php the_author(); ?></a></li>
                <?php endif; 
                if($archive_date_on) : ?>
                	<li> <i class="far fa-calendar-alt"></i><?php echo get_the_date('F j, Y'); ?></li>
                <?php endif;
                if($archive_tags_on && has_tag()) : ?>
                	<li><i class="fas fa-tag"></i><?php the_tags('',' , '); ?></li>
                <?php endif; ?>
            </ul>
		<?php endif; 
	}
endif;

/**
 * Prints post meta on blog
*/
if ( ! function_exists( 'digicrew_post_meta' ) ) :
    function digicrew_post_meta() {
		$post_an_on = digicrew_get_opt( 'post_an_on', true );
        $post_tags_on = digicrew_get_opt( 'post_tags_on', true );
        $post_category_on = digicrew_get_opt( 'post_category_on', true );
		$post_date_on = digicrew_get_opt( 'post_date_on', true );
        if( $post_an_on || $post_tags_on || $post_date_on || $post_category_on) : ?>
        	<ul class="blog-meta">
        		<?php if($post_an_on) : ?>
	            	<li><a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID')),'digicrew'); ?>"><i class="fas fa-user"></i> <?php the_author(); ?></a></li>
	            <?php endif; 
                if($post_date_on) : ?>
	            	<li> <i class="far fa-calendar-alt"></i> <?php the_date(); ?></li>
	            <?php endif;
                if($post_tags_on && has_tag()) : ?>
	            	<li><i class="fas fa-tag"></i><?php the_tags('',' , '); ?></li>
	            <?php endif; 
                if($post_category_on && has_category()) : ?>
                    <li><i class="fas fa-tag"></i><?php the_category(' , '); ?></li>
                <?php endif; ?>
	        </ul>
	    <?php endif; 
	}
endif; ?>