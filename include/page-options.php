<?php
/**
 * Register metabox for posts based on Redux Framework. Supported methods:
 *     isset_args( $post_type )
 *     set_args( $post_type, $redux_args, $metabox_args )
 *     add_section( $post_type, $sections )
 * Each post type can contains only one metabox. Pease note that each field id
 * leads by an underscore sign ( _ ) in order to not show that into Custom Field
 * Metabox from WordPress core feature.
 *
 * @param  Digicrew_Post_Metabox $metabox
 */

/**
 * Get list menu.
 * @return array
*/
function digicrew_get_nav_menu(){

    $menus = array(
        '' => esc_html__('Default', 'digicrew')
    );

    $obj_menus = wp_get_nav_menus();

    foreach ($obj_menus as $obj_menu){
        $menus[$obj_menu->term_id] = $obj_menu->name;
    }

    return $menus;
}

function digicrew_page_options_register( $metabox ) {
	if ( ! $metabox->isset_args( 'post' ) ) {
		$metabox->set_args( 'post', array(
			'opt_name'            => 'post_option',
			'display_name'        => esc_html__( 'Post Settings', 'digicrew' ),
			'show_options_object' => false,
		), 
		array(
			'context'             => 'advanced',
			'priority' 			  => 'default'
		) );
	}

	if ( ! $metabox->isset_args( 'page' ) ) {
		$metabox->set_args( 'page', array(
			'opt_name'            => digicrew_get_page_opt_name(),
			'display_name'        => esc_html__( 'Page Settings', 'digicrew' ),
			'show_options_object' => false,
		), array(
			'context'  			  => 'advanced',
			'priority' 			  => 'default'
		) );
	}

	/**
	 * Config page meta options
	 *
	*/

	$metabox->add_section( 'page', array(
		'title'  => esc_html__( 'Header', 'digicrew' ),
		'desc'   => esc_html__( 'Header settings for the page.', 'digicrew' ),
		'icon'   => 'el-icon-website',
		'fields' => array(
			array(
				'id'      => 'custom_header',
				'type'    => 'switch',
		        'desc'    => esc_html__( 'Header settings for the page.', 'digicrew' ),
				'title'   => esc_html__( 'Custom Header', 'digicrew' ),
				'default' => false,
				'indent'  => true
			),

			array(
	            'id'       => 'page_logo',
	            'type'     => 'media',
	            'title'    => esc_html__('Logo', 'digicrew'),
	            'default'  => array(
	                'url'  =>get_template_directory_uri().'/assets/images/logo.png'
	            ),
	            'required' => array( 
	            	0 => 'custom_header', 
	            	1 => 'equals', 2 => '1' 
	            ),
	        ),
  
			array(
	            'id'          => 'page_color',
	            'type'        => 'color',
	            'title'       => esc_html__('Color', 'digicrew'),
	            'transparent' => false,
	            'default'     => '#ffc80a',
	            'required'    => array( 
	            	0 => 'custom_header', 
	            	1 => 'equals', 
	            	2 => '1' ),
	        ), 
	        array(
				'id'      => 'h_menu',
				'type'    => 'switch',
				'title'   => esc_html__( 'Header Transparent', 'digicrew' ),
				'default' => true,
			),
		)
	) );

	$metabox->add_section( 'page', array(
		'title'  => esc_html__( 'Page Title', 'digicrew' ),
		'icon'   => 'el-icon-map-marker',
		'fields' => array(
			array(
	            'id'        => 'ptitle_on',
	            'type'      => 'button_set',
	            'title'     => esc_html__('Displayed', 'digicrew'),
	            'options'   => array(
	                'show'  => esc_html__('Show', 'digicrew'),
	                'hidden'=> esc_html__('Hidden', 'digicrew'),
	            ),
	            'default'   => 'show'
	        ),

	        array(
	            'id'       => 'p_subtitle',
	            'type'     => 'textarea',
	            'title'    => esc_html__( 'Page SubTitle', 'digicrew' ),
	            'default'  => ' ',
	            'required' => array( 
	            	0 => 'ptitle_on', 
	            	1 => 'equals', 
	            	2 => 'show' 
	            ),
	        ), 
			array(
				'id'           => 'ptitle_background',
				'type'         => 'background',
				'title'        => esc_html__('Background', 'digicrew'),
				'output'       => array('background'=> '.page-banner'),
				'required'     => array( 
										0 => 'ptitle_on', 
										1 => 'equals', 
										2 => 'show' 
									),
				'force_output' => true,
				'default'      => array(
					'background-repeat'     => 'no-repeat',
					'background-position'   => 'center top',
					'background-attachment' => 'fixed',
					'background-image'      => get_template_directory_uri().'/assets/images/banner.jpg',

				),
				'background-color' =>false,
			),
		)
	) );

	$metabox->add_section( 'page', array(
		'title'  => esc_html__( 'Content', 'digicrew' ),
		'desc'   => esc_html__( 'Settings for content area.', 'digicrew' ),
		'icon'   => 'el-icon-pencil',
		'fields' => array(

			array(
				'id'                 => 'content_padding',
				'type'               => 'spacing',
				'output'             => array( '#content' ),
				'right'              => false,
				'left'               => false,
				'mode'               => 'padding',
				'units'              => array( 'px' ),
				'units_extended'     => 'false',
				'title'              => esc_html__( 'Content Padding', 'digicrew' ),
				'desc'               => esc_html__( 'Default: Theme Option.', 'digicrew' ),
				'default'            => array(
					'padding-top'    => '',
					'padding-bottom' => '',
					'units'          => 'px',
				)
			),
			array(
				'id'      => 'show_sidebar_page',
				'type'    => 'switch',
				'title'   => esc_html__( 'Show Sidebar', 'digicrew' ),
				'default' => false,
				'indent'  => true
			),
			array(
				'id'           => 'sidebar_page_pos',
				'type'         => 'button_set',
				'title'        => esc_html__( 'Sidebar Position', 'digicrew' ),
				'options'      => array(
					'left'     => esc_html__( 'Left', 'digicrew' ),
					'right'    => esc_html__( 'Right', 'digicrew' ),
				),
				'default'      => 'right',
				'required'     => array( 
									0 => 'show_sidebar_page', 
									1 => '=', 2 => '1' 
								),
				'force_output' => true
			),
		)
	) );
}

add_action( 'cms_post_metabox_register', 'digicrew_page_options_register' );

function digicrew_get_option_of_theme_options( $key, $default = '' ) {
	if ( empty( $key ) ) {
		return '';
	}
	$options = get_option( digicrew_get_opt_name(), array() );
	$value   = isset( $options[ $key ] ) ? $options[ $key ] : $default;
	return $value;
}