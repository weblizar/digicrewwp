<?php
if ( ! class_exists( 'ReduxFrameworkInstances' ) ) {
    return;
}

/*
 * Convert HEX to GRBA
 */
if(!function_exists('digicrew_rgba')){
    function digicrew_rgba($hex,$opacity = 1) {
        $hex = str_replace("#",null, $hex);
        $color = array();
        if(strlen($hex) == 3) {
            $color['r'] = hexdec(substr($hex,0,1).substr($hex,0,1));
            $color['g'] = hexdec(substr($hex,1,1).substr($hex,1,1));
            $color['b'] = hexdec(substr($hex,2,1).substr($hex,2,1));
            $color['a'] = $opacity;
        }
        else if(strlen($hex) == 6) {
            $color['r'] = hexdec(substr($hex, 0, 2));
            $color['g'] = hexdec(substr($hex, 2, 2));
            $color['b'] = hexdec(substr($hex, 4, 2));
            $color['a'] = $opacity;
        }
        $color = "rgba(".implode(', ', $color).")";
        return $color;
    }
}

/*
 * Convert HEX to Dark & Lighten
 */
function digicrew_lighten( $hex, $percent ) {
    
    // validate hex string
    
    $hex = preg_replace( '/[^0-9a-f]/i', '', $hex );
    $new_hex = '#';
    
    if ( strlen( $hex ) < 6 ) {
        $hex = $hex[0] + $hex[0] + $hex[1] + $hex[1] + $hex[2] + $hex[2];
    }
    
    // convert to decimal and change luminosity
    for ($i = 0; $i < 3; $i++) {
        $dec = hexdec( substr( $hex, $i*2, 2 ) );
        $dec = min( max( 0, $dec + $dec * $percent ), 255 ); 
        $new_hex .= str_pad( dechex( $dec ) , 2, 0, STR_PAD_LEFT );
    }       
    
    return $new_hex;
}

class CSS_Generator {
    /**
     * scssc class instance
     *
     * @access protected
     * @var scssc
     */
    protected $scssc = null;

    /**
     * ReduxFramework class instance
     *
     * @access protected
     * @var ReduxFramework
     */
    protected $redux = null;

    /**
     * Debug mode is turn on or not
     *
     * @access protected
     * @var boolean
     */
    protected $dev_mode = true;

    /**
     * opt_name of ReduxFramework
     *
     * @access protected
     * @var string
     */
    protected $opt_name = '';


    /**
     * Constructor
     */
    function __construct() {
        $this->opt_name = digicrew_get_opt_name();

        if ( empty( $this->opt_name ) ) {
            return;
        }
        $this->dev_mode = digicrew_get_opt( 'dev_mode', '0' ) === '1' ? true : false;
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ), 10 );        
    }

    /**
     * Hooked wp_enqueue_scripts - 20
     * Make sure that the handle is enqueued from earlier wp_enqueue_scripts hook.
     */
    function enqueue() {
        
        $css = $this->inline_css();
        // wp_add_inline_style( 'digicrew-theme-abc', $css  );
        

        if ( !empty($css) ) {
            wp_add_inline_style( 'digicrew-style', $this->dev_mode ? $css : digicrew_css_minifier( $css ) );
        }
    }

    /**
     * Generate inline css based on theme options
     */
    protected function inline_css() {
        ob_start();
        
        /* Logo */
        $logo_maxh = digicrew_get_opt( 'logo_maxh' );

        if (!empty($logo_maxh['height']) && $logo_maxh['height'] != 'px')
        {
            printf( '.header-one .navbar .navbar-brand img{ max-height: %s; }', esc_attr($logo_maxh['height']) );
        }

        $color          = digicrew_get_opt( 'primary_color' ,'');
        $custom_header  = digicrew_get_page_opt( 'custom_header', '0');
        if ( is_page() && $custom_header == '1' ) {
            $page_color = digicrew_get_page_opt('page_color');
            $color      = $page_color;
            
            if($color == '0') {
                return;
            }
        }
        if(!empty($color)){
            echo "
                .gallery-filter-btn button:hover,.gallery-filter-btn button:focus, .gallery-filter-btn button.active {
                    border-color:" . esc_attr($color) . ";
                }
                .log-btn a,.hero-area .btn-theme ,.slick-prev, .slick-next,
                .btn-1,.comment-form button.btn,.prev.page-numbers,.next.page-numbers{
                    border: 2px solid " . esc_attr($color) . "!important;
                }

                .testi-slide .slick-current .item .img-nav img,
                .pricingTable.active .read, .pricingTable:hover .read,.pricingTable:focus .read, .pricingTable .read:hover, .pricingTable .read:focus,.left-comment-box img,.video-btn a {
                    border: solid 4px " . esc_attr($color) . ";
                }

                .btn-1:hover,.btn-1:focus,#btn-to-top,.navbar-light .navbar-nav .nav-item.megamenu .dropdown .title::before,.log-btn a:hover,.log-btn a:focus,
                .hero-area .btn-theme:hover,.hero-area .btn-theme:focus, .hero-area .btn-theme.btn-active,
                .seo-analysis-form button,.icon-slide .slick-dots li.slick-active button ,
                .slick-prev:hover,.slick-prev:focus, .slick-next:hover, .slick-next:focus,
                .testi-slide.testimonial-slider-nav .slick-current .item .img-nav::after,
                .team-box .team-overlay .team-info .social-icon::before,
                .team-box .team-overlay .team-info .social-icon::after,
                .search-box .btn-search,.tags-btn a:hover ,.tags-btn a:focus ,.comment-form button.btn:hover,.comment-form button.btn:focus,
                .more-less,.contact-form .btn-1:hover,.contact-form .btn-1:focus,.btn-1:hover,.btn-1:focus, .faq-form .btn-1:hover, .faq-form .btn-1:focus,
                .pagination .page-numbers.current, .pagination .post-page-numbers.current,
                .seo-analysis-form .btn,.wp-block-search .wp-block-search__button,
                .tagcloud a:hover,.wp-block-tag-cloud a:hover,.tagcloud a:focus,.wp-block-tag-cloud a:focus ,.next.page-numbers:hover, .prev.page-numbers:hover,
                .next.page-numbers:focus, .prev.page-numbers:focus{
                    background-color: " . esc_attr($color) . ";
                }
                .navbar-light .navbar-nav .nav-link:hover,.navbar-light .navbar-nav .nav-link:focus, .navbar-light .navbar-nav .nav-link:hover, .navbar-light .navbar-nav .nav-link:focus, .navbar-light .navbar-nav .active>.nav-link ,
                .navbar-light .navbar-nav .nav-item .dropdown .nav-link:hover,
                .navbar-light .navbar-nav .nav-item .dropdown .nav-link:focus,
                .slicknav_nav .slicknav_row:hover,
                .slicknav_nav .slicknav_row:focus, .slicknav_nav a:hover, .slicknav_nav a:focus,.copyright-area .copyright-nav  .nav-item .dropdown .nav-link:hover,
                .copyright-area .copyright-nav  .nav-item .dropdown .nav-link:focus 
                .team-box .team-overlay .team-info .social-icon li>a:hover ,
                .team-box .team-overlay .team-info .social-icon li>a:focus ,
                .blog-box .blog-content .read-btn:hover,
                .blog-box .blog-content .read-btn:focus, .blog-box .blog-content h3>a:hover, .blog-box .blog-content h3>a:focus,
                .blog-box .blog-content .blog-meta li>a:hover,
                .blog-box .blog-content .blog-meta li>a:focus,
                .copyright-area .copyright-nav li a:hover,
                .copyright-area .copyright-nav li a:focus, .footer-info p>a:hover, .footer-info p>a:focus, .footer-info .social-icon li>a:hover, .footer-info .social-icon li>a:focus, .footer-box .footer-list li>a:hover, .footer-box .footer-list li>a:focus,
                .breadcrumb-item.active,.main-timeline .timeline-content:hover .title,.main-timeline .timeline-content:focus .title,
                .recent-post .post-right h5:hover,
                .recent-post .post-right h5:focus,.share-icon li:hover,.share-icon li:focus,
                .comment-box .right-comment-content>a:hover,
                .comment-box .right-comment-content>a:focus ,.footer-box .post-right a:hover,.footer-box .post-right a:focus,.search-box .btn-search,.wp-calendar-nav a:hover,.wp-calendar-nav a:focus  {
                    color: " . esc_attr($color) . ";
                }
            ";
        }        

        /* Custom Css */
        $custom_css = digicrew_get_opt( 'site_css' );
        if(!empty($custom_css)) { echo esc_attr($custom_css); }
        return ob_get_clean();
    }
    protected function print_css( $color ) {

    }
}
new CSS_Generator();