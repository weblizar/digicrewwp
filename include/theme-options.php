<?php
if ( ! class_exists( 'ReduxFramework' ))  {
    return;
}

if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
    remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
}

if ( class_exists( 'Newsletter' ) ) {
    $forms = array_filter( (array) get_option( 'newsletter_forms', array() ) );

    $newsletter_forms = array(
        'default' => esc_html__( 'Default Form', 'digicrew' )
    );

    if ( $forms ) {
        $index = 1;
        foreach ( $forms as $key => $form )
        {
                /* translators: 1: theme name(s). */
            $newsletter_forms[ $key ] = sprintf( esc_html__( 'Form %s', 'digicrew' ), $index );
            $index ++;
        }
    }
} else {
    $newsletter_forms = '';
}

$opt_name = digicrew_get_opt_name();
$theme    = wp_get_theme();

$args     = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name'             => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name'         => $theme->get('Name'),
    // Name that appears at the top of your panel
    'display_version'      => $theme->get('Version'),
    // Version that appears at the top of your panel
    'menu_type'            => class_exists('DigicrewAddons') ? 'menu' : '',
    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu'       => true,
    // Show the sections below the admin menu item or not
    'menu_title'           => esc_html__( 'Digicrew Options', 'digicrew' ),
    'page_title'           => esc_html__( 'Digicrew Options', 'digicrew' ),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key'       => '',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography'     => false,
    // Use a asynchronous font on the front end or font string
    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
    'admin_bar'            => true,
    // Show the panel pages on the admin bar
    'admin_bar_icon'       => 'dashicons-smartphone',
    // Choose an icon for the admin bar menu
    'admin_bar_priority'   => 50,
    // Choose an priority for the admin bar menu
    'global_variable'      => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode'             => true,
    // Show the time the page took to load, etc
    'update_notice'        => true,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer'           => true,
    // Enable basic customizer support
    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field
    'show_options_object'  => false,
    // OPTIONAL -> Give you extra features
    'page_priority'        => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent'          => class_exists('DigicrewAddons') ? $theme->get('TextDomain') : '',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions'     => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon'            => 'dashicons-smartphone',
    // Specify a custom URL to an icon
    'last_tab'             => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon'            => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug'            => 'theme-options',
    // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
    'save_defaults'        => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show'         => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark'         => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export'   => true,
    // Shows the Import/Export panel when not used as a field.

    // CAREFUL -> These options are for advanced use only
    'transient_time'       => 60 * MINUTE_IN_SECONDS,
    'output'               => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag'           => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database'             => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
    'use_cdn'              => true,
    // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

    // HINTS
    'hints'                => array(
        'icon'             => 'el el-question-sign',
        'icon_position'    => 'right',
        'icon_color'       => 'lightgray',
        'icon_size'        => 'normal',
        'tip_style'        => array(
            'color'        => 'red',
            'shadow'       => true,
            'rounded'      => false,
            'style'        => '',
        ),
        'tip_position'     => array(
            'my'           => 'top left',
            'at'           => 'bottom right',
        ),
        'tip_effect'       => array(
            'show'         => array(
                'effect'   => 'slide',
                'duration' => '500',
                'event'    => 'mouseover',
            ),
            'hide'         => array(
                'effect'   => 'slide',
                'duration' => '500',
                'event'    => 'click mouseleave',
            ),
        ),
    ),
    'templates_path'       => class_exists('DigicrewAddons') ? Digicrewaddons()->path('APP_DIR') . '/templates/redux/' : '',
);

Redux::SetArgs($opt_name, $args);

/*--------------------------------------------------------------
# General
--------------------------------------------------------------*/

Redux::setSection( $opt_name, array(
    'title'            => esc_html__( 'General', 'digicrew' ),
    'icon'             => 'el-icon-home',
    'fields'           => array(
        array(
            'id'       => 'show_page_loading',
            'type'     => 'switch',
            'title'    => esc_html__( 'Enable Page Loading', 'digicrew' ),
            'subtitle' => esc_html__( 'Enable page loading effect when you load site.', 'digicrew' ),
            'default'  => true
        ),
        array(
            'id'       => 'gototop',
            'type'     => 'switch',
            'title'    => esc_html__( 'Go To Top Button', 'digicrew' ),
            'default'  => true
        ),
        
    )
));

/*--------------------------------------------------------------
# Header
--------------------------------------------------------------*/

Redux::setSection($opt_name, array(
    'title'  => esc_html__('Header', 'digicrew'),
    'icon'   => 'el-icon-website',
    'fields' => array(
        
        array(
            'id'       => 'sticky_on',
            'type'     => 'switch',
            'title'    => esc_html__('Sticky Header', 'digicrew'),
            'subtitle' => esc_html__('Header will be sticked when applicable.', 'digicrew'),
            'default'  => true
        ),
        array(
            'id'       => 'btn_txt',
            'type'     => 'text',
            'title'    => esc_html__( 'Button Text', 'digicrew' ),
            'desc'     => ' ',
            'default'  => '',
        ),     
        array(
            'id'       => 'btn_url',
            'type'     => 'text',
            'title'    => esc_html__( 'Button Url', 'digicrew' ),
            'desc'     => '',
            'default'  => '',
        ),    
    )
));

Redux::setSection($opt_name, array(
    'title'      => esc_html__('Logo', 'digicrew'),
    'icon'       => 'el el-picture',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'logo',
            'type'     => 'media',
            'title'    => esc_html__('Logo', 'digicrew'),
            'default'  => array(
            'url'      => get_template_directory_uri().'/assets/images/logo.png'
            )
        ),

        array(
            'id'       => 'logo_maxh',
            'type'     => 'dimensions',
            'title'    => esc_html__('Logo Max height', 'digicrew'),
            'subtitle' => esc_html__('Set maximum height for your logo, just in case the logo is too large.', 'digicrew'),
            'desc'           => esc_html__('Default: 54px', 'digicrew'),
            'width'    => false,
            'unit'     => 'px'
        ),
    )
));

Redux::setSection($opt_name, array(
    'title'      => esc_html__('Navigation', 'digicrew'),
    'icon'       => 'el el-lines',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'          => 'font_menu',
            'type'        => 'typography',
            'title'       => esc_html__('Custom Google Font', 'digicrew'),
            'google'      => true,
            'font-backup' => true,
            'all_styles'  => true,
            'font-style'  => true,
            'font-weight' => true,
            'text-transform' => true,
            'text-align'  => true,
            'font-size'   => true,
            'line-height' => false,
            'color'       => true,
            'output'      => array('.navbar-nav .nav-item a, .navbar-nav .dropdown-item a, .navbar-expand-lg .navbar-nav .nav-link,.navbar-nav .dropdown-menu .dropdown-item a'),
            'units'       => 'px',
        ),
        
    )
));
/*--------------------------------------------------------------
# WordPress default Page Title
--------------------------------------------------------------*/
Redux::setSection($opt_name, array(
    'title'         => esc_html__('Page Title', 'digicrew'),
    'icon'          => 'el el-map-marker',
    'fields'        => array(
        array(
            'id'          => 'page_title_on',
            'type'        => 'button_set',
            'title'       => esc_html__('Displayed', 'digicrew'),
            'options'     => array(
                'show'    => esc_html__('Show', 'digicrew'),
                'hidden'  => esc_html__('Hidden', 'digicrew'),
            ),
            'default'     => 'show'
        ),

        array(
            'id'       => 'page_subtitle',
            'type'     => 'textarea',
            'title'    => esc_html__( 'Page SubTitle', 'digicrew' ),
            'desc'     => '',
            'default'  => '',
        ),          

        array(
            'id'                            => 'page_title_background',
            'type'                          => 'background',
            'title'                         => esc_html__('Background', 'digicrew'),
            'output'                        => array('background'=> '.banner-bg'),
            'required'                      => array( 
                                                    0 => 'page_title_on', 
                                                    1 => 'equals', 
                                                    2 => 'show' 
                                                ),
            'force_output'                  => true,
            'default'                       => array(
                'background-repeat'         => 'no-repeat',
                'background-position'       => 'center top',
                'background-attachment'     => 'fixed',
                'background-image'          => get_template_directory_uri().'/assets/images/banner.jpg',
            ),
            'background-color'              =>false,
            'output'                        => array('background'=> '.banner-bg'),
        ),
    )
));

/*--------------------------------------------------------------
# WordPress default content
--------------------------------------------------------------*/

Redux::setSection($opt_name, array(
    'title' => esc_html__('Content', 'digicrew'),
    'icon'  => 'el-icon-pencil',
    'fields'=> array(

        array(
            'id'                 => 'content_padding',
            'type'               => 'spacing',
            'output'             => array('#content'),
            'right'              => false,
            'left'               => false,
            'mode'               => 'padding',
            'units'              => array('px'),
            'units_extended'     => 'false',
            'title'              => esc_html__('Content Padding', 'digicrew'),
            'desc'               => esc_html__('Default: Top-100px, Bottom-100px', 'digicrew'),
            'default'            => array(
                'padding-top'    => '',
                'padding-bottom' => '',
                'units'          => 'px',
            )
        )
    )
));


Redux::setSection($opt_name, array(
    'title'      => esc_html__('Blog / Archive', 'digicrew'),
    'icon'       => 'el-icon-list',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'archive_sidebar_pos',
            'type'     => 'button_set',
            'title'    => esc_html__('Sidebar Position', 'digicrew'),
            'subtitle' => esc_html__('Select a sidebar position for blog home, archive, search...', 'digicrew'),
            'options'  => array(
                'left' => esc_html__('Left', 'digicrew'),
                'right'=> esc_html__('Right', 'digicrew'),
                'none' => esc_html__('Disabled', 'digicrew')
            ),
            'default'  => 'right'
        ),
        array(
            'id'       => 'archive_date_on',
            'title'    => esc_html__('Date', 'digicrew'),
            'subtitle' => esc_html__('Show date posted on each post.', 'digicrew'),
            'type'     => 'switch',
            'default'  => true,
        ),
        array(
            'id'       => 'archive_author_on',
            'title'    => esc_html__('Author', 'digicrew'),
            'subtitle' => esc_html__('Show author name on each post.', 'digicrew'),
            'type'     => 'switch',
            'default'  => true,
        ),
        array(
            'id'       => 'archive_tags_on',
            'title'    => esc_html__('Tags', 'digicrew'),
            'subtitle' => esc_html__('Show tags on each post.', 'digicrew'),
            'type'     => 'switch',
            'default'  => false,
        ),
    )
));

Redux::setSection($opt_name, array(
    'title'      => esc_html__('Single Post', 'digicrew'),
    'icon'       => 'el-icon-file-edit',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'        => 'post_sidebar_pos',
            'type'      => 'button_set',
            'title'     => esc_html__('Sidebar Position', 'digicrew'),
            'subtitle'  => esc_html__('Select a sidebar position', 'digicrew'),
            'options'   => array(
                'left'  => esc_html__('Left', 'digicrew'),
                'right' => esc_html__('Right', 'digicrew'),
                'none'  => esc_html__('Disabled', 'digicrew')
            ),
            'default'   => 'right'
        ),
        array(
            'id'       => 'post_feature_image_on',
            'title'    => esc_html__('Feature Image', 'digicrew'),
            'subtitle' => esc_html__('Show feature image on single post.', 'digicrew'),
            'type'     => 'switch',
            'default'  => true
        ),
        array(
            'id'       => 'post_date_on',
            'title'    => esc_html__('Date', 'digicrew'),
            'subtitle' => esc_html__('Show date on single post.', 'digicrew'),
            'type'     => 'switch',
            'default'  => true
        ),
        array(
            'id'       => 'post_an_on',
            'title'    => esc_html__('Author Name', 'digicrew'),
            'subtitle' => esc_html__('Show category names on single post.', 'digicrew'),
            'type'     => 'switch',
            'default'  => true
        ),
        array(
            'id'       => 'post_tags_on',
            'title'    => esc_html__('Tags', 'digicrew'),
            'subtitle' => esc_html__('Show tags count on single post.', 'digicrew'),
            'type'     => 'switch',
            'default'  => true
        ),
        array(
            'id'       => 'post_category_on',
            'title'    => esc_html__('Category', 'digicrew'),
            'subtitle' => esc_html__('Show Category on single post.', 'digicrew'),
            'type'     => 'switch',
            'default'  => true
        ),
        array(
            'id'       => 'post_social_share_on',
            'title'    => esc_html__('Social Share', 'digicrew'),
            'subtitle' => esc_html__('Show social share on single post.', 'digicrew'),
            'type'     => 'switch',
            'default'  => false,
        ),
        array(
            'id'       => 'post_comments_form_on',
            'title'    => esc_html__('Comments Form', 'digicrew'),
            'subtitle' => esc_html__('Show comments form on single post.', 'digicrew'),
            'type'     => 'switch',
            'default'  => true
        ),
    )
));

/*--------------------------------------------------------------
# Footer
--------------------------------------------------------------*/

Redux::setSection($opt_name, array(
    'title'  => esc_html__('Footer', 'digicrew'),
    'icon'   => 'el el-website',
    'fields' => array(

        array(
            'id'       => 'footer-select-col',
            'type'     => 'image_select',
            'title'    => esc_html__( 'Footer Layout', 'digicrew' ),
            'subtitle' => '',
            'desc'     =>  '',
            //Must provide key => value pairs for select options
            'options'  => array(
                '6'    => array(
                    'title' => '2 Columns', 
                    'img' => get_template_directory_uri().'/assets/images/2col.png'
                ),
                '4'    => array(
                    'title' => '3 Columns', 
                    'img' => get_template_directory_uri().'/assets/images/3col.png'
                ),
                '3'    => array(
                    'title' => '4 Columns', 
                    'img' => get_template_directory_uri().'/assets/images/4col.png'
                ),
            ),
            'default'  => '2'
        ),
        
        array(
            'id'                     => 'nf-img',
            'type'                   => 'background',
            'title'                  => esc_html__( 'Background Image', 'digicrew' ),
            'output'                 => array('background'=> '.footer-bg'),
            'background-color'       => false, 
            'background-position'    => false,
            'background-attachment'  => false,
            'background-repeat'      => false,
            'background-clip'        => false,
            'background-size'        => false,
            'force_output'           => true,
            'default'                => array(
                'background-image'   => get_template_directory_uri().'/assets/images/foot-bg.jpg',
            ),
        ),
        
        array(
            'id'          =>'footer_copyright',
            'type'        => 'textarea',
            'title'       => esc_html__('Copyright', 'digicrew'),
            'validate'    => 'html_custom',
            'default'     => '<p>&copy; 2020 Digicrew. All Rights Reserved</p>',
            'subtitle'    => esc_html__('Custom HTML Allowed: a,br,em,strong,span,p,div,h1->h6', 'digicrew'),
            'allowed_html'=> array(
                'a'       => array(
                'href'    => array(),
                'title'   => array(),
                'class'   => array(),
                ),
                'br'      => array(),
                'em'      => array(),
                'strong'  => array(),
                'span'    => array(),
                'p'       => array(),
                'div'     => array(
                'class'   => array()
                ),
                'h1'      => array(
                    'class'=> array()
                ),
                'h2'        => array(
                    'class' => array()
                ),
                'h3'        => array(
                    'class' => array()
                ),
                'h4'        => array(
                    'class' => array()
                ),
                'h5'        => array(
                    'class' => array()
                ),
                'h6'        => array(
                    'class' => array()
                ),
                'ul'        => array(
                    'class' => array()
                ),
                'li' => array(),
            ),
            'force_output' => true
        ),
    )
));


/*--------------------------------------------------------------
# Colors
--------------------------------------------------------------*/

Redux::setSection($opt_name, array(
    'title'  => esc_html__('Colors', 'digicrew'),
    'icon'   => 'el-icon-file-edit',
    'fields' => array(
        array(
            'id'          => 'primary_color',
            'type'        => 'color',
            'title'       => esc_html__('Color', 'digicrew'),
            'transparent' => false,
            'default'     => '#ffc80a'
        ),
       
    )
));

/*--------------------------------------------------------------
# Typography
--------------------------------------------------------------*/
$custom_font_selectors_1 = Redux::getOption($opt_name, 'custom_font_selectors_1');
$custom_font_selectors_1 = !empty($custom_font_selectors_1) ? explode(',', $custom_font_selectors_1) : array();
Redux::setSection($opt_name, array(
    'title'  => esc_html__('Typography', 'digicrew'),
    'icon'   => 'el-icon-text-width',
    'fields' => array(
        array(
            'id'               => 'body_default_font',
            'type'             => 'select',
            'title'            => esc_html__('Body Default Font', 'digicrew'),
            'options'          => array(
                'Montserrat'   => esc_html__('Default', 'digicrew'),
                'Google-Font'  => esc_html__('Google Font', 'digicrew'),
            ),
            'default'          => 'Montserrat',
        ),
        array(
            'id'          => 'font_main',
            'type'        => 'typography',
            'title'       => esc_html__('Body Google Font', 'digicrew'),
            'subtitle'    => esc_html__('This will be the default font of your website.', 'digicrew'),
            'google'      => true,
            'font-backup' => true,
            'all_styles'  => true,
            'line-height' => true,
            'font-size'   => true,
            'text-align'  => false,
            'color'       => false,
            'output'      => array('body'),
            'units'       => 'px',
            'required'    => array( 
                0 => 'body_default_font', 
                1 => 'equals', 
                2 => 'Google-Font' 
            ),
            'force_output'=> true
        ),
        array(
            'id'          => 'body_color',
            'type'        => 'color',
            'title'       => esc_html__('Body Color', 'digicrew'),
            'transparent' => false,
            'default'     => '',
            'required'    => array( 
                0 => 'body_default_font', 
                1 => 'equals', 
                2 => 'Google-Font' 
            ),
            'force_output'=> true,
            'output'      => array('body'),
        ),
        array(
            'id'       => 'heading_default_font',
            'type'     => 'select',
            'title'    => esc_html__('Heading Default Font', 'digicrew'),
            'options'  => array(
                'Montserrat'  => esc_html__('Default', 'digicrew'),
                'Google-Font'  => esc_html__('Google Font', 'digicrew'),
            ),
            'default'  => 'Montserrat',
        ),
        array(
            'id'          => 'font_h1',
            'type'        => 'typography',
            'title'       => esc_html__('H1', 'digicrew'),
            'subtitle'    => esc_html__('This will be the default font for all H1 tags of your website.', 'digicrew'),
            'google'      => true,
            'font-backup' => true,
            'all_styles'  => true,
            'text-align'  => false,
            'color'       => true,
            'output'      => array('h1', '.h1', '.text-heading'),
            'units'       => 'px',
            'required' => array( 
                0 => 'heading_default_font', 
                1 => 'equals', 
                2 => 'Google-Font' 
            ),
            'force_output' => true
        ),
        array(
            'id'          => 'font_h2',
            'type'        => 'typography',
            'title'       => esc_html__('H2', 'digicrew'),
            'subtitle'    => esc_html__('This will be the default font for all H2 tags of your website.', 'digicrew'),
            'google'      => true,
            'font-backup' => true,
            'all_styles'  => true,
            'text-align'  => false,
            'color'       => true,
            'output'      => array('h2', '.h2'),
            'units'       => 'px',
            'required' => array( 
                0 => 'heading_default_font', 
                1 => 'equals', 
                2 => 'Google-Font' 
            ),
            'force_output' => true
        ),
        array(
            'id'          => 'font_h3',
            'type'        => 'typography',
            'title'       => esc_html__('H3', 'digicrew'),
            'subtitle'    => esc_html__('This will be the default font for all H3 tags of your website.', 'digicrew'),
            'google'      => true,
            'font-backup' => true,
            'all_styles'  => true,
            'text-align'  => false,
            'color'       => true,
            'output'      => array('h3', '.h3'),
            'units'       => 'px',
            'required'    => array( 
                0 => 'heading_default_font', 
                1 => 'equals', 
                2 => 'Google-Font' 
            ),
            'force_output'=> true
        ),
        array(
            'id'          => 'font_h4',
            'type'        => 'typography',
            'title'       => esc_html__('H4', 'digicrew'),
            'subtitle'    => esc_html__('This will be the default font for all H4 tags of your website.', 'digicrew'),
            'google'      => true,
            'font-backup' => true,
            'all_styles'  => true,
            'text-align'  => false,
            'color'       => true,
            'output'      => array('h4', '.h4'),
            'units'       => 'px',
            'required'    => array( 
                0 => 'heading_default_font', 
                1 => 'equals', 
                2 => 'Google-Font' 
            ),
            'force_output'=> true
        ),
        array(
            'id'          => 'font_h5',
            'type'        => 'typography',
            'title'       => esc_html__('H5', 'digicrew'),
            'subtitle'    => esc_html__('This will be the default font for all H5 tags of your website.', 'digicrew'),
            'google'      => true,
            'font-backup' => true,
            'all_styles'  => true,
            'text-align'  => false,
            'color'       => true,
            'output'      => array('h5', '.h5'),
            'units'       => 'px',
            'required'    => array( 
                0 => 'heading_default_font', 
                1 => 'equals', 
                2 => 'Google-Font' 
            ),
            'force_output'=> true
        ),
        array(
            'id'          => 'font_h6',
            'type'        => 'typography',
            'title'       => esc_html__('H6', 'digicrew'),
            'subtitle'    => esc_html__('This will be the default font for all H6 tags of your website.', 'digicrew'),
            'google'      => true,
            'font-backup' => true,
            'all_styles'  => true,
            'text-align'  => false,
            'color'       => true,
            'output'      => array('h6', '.h6'),
            'units'       => 'px',
            'required'    => array( 
                0 => 'heading_default_font', 
                1 => 'equals', 
                2 => 'Google-Font' 
            ),
            'force_output'=> true
        )
    )
));

/* Custom Code /--------------------------------------------------------- */
Redux::setSection($opt_name, array(
    'title'  => esc_html__('Custom Code', 'digicrew'),
    'icon'   => 'el-icon-edit',
    'fields' => array(

        array(
            'id'       => 'site_header_code',
            'type'     => 'textarea',
            'title'    => esc_html__('Header Custom Codes', 'digicrew'),
            'subtitle' => esc_html__('It will insert the code to wp_head hook.', 'digicrew'),
        ),
        array(
            'id'       => 'site_footer_code',
            'type'     => 'textarea',
            'title'    => esc_html__('Footer Custom Codes', 'digicrew'),
            'subtitle' => esc_html__('It will insert the code to wp_footer hook.', 'digicrew'),
        ),

    ),
));

/* Custom CSS /--------------------------------------------------------- */
Redux::setSection($opt_name, array(
    'title'  => esc_html__('Custom CSS', 'digicrew'),
    'icon'   => 'el-icon-adjust-alt',
    'fields' => array(

        array(
            'id'   => 'customcss',
            'type' => 'info',
            'desc' => esc_html__('Custom CSS', 'digicrew')
        ),

        array(
            'id'       => 'site_css',
            'type'     => 'ace_editor',
            'title'    => esc_html__('CSS Code', 'digicrew'),
            'subtitle' => esc_html__('Advanced CSS Options. You can paste your custom CSS Code here.', 'digicrew'),
            'mode'     => 'css',
            'validate' => 'css',
            'default'  => ""
        ),

    ),
));