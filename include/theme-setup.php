<?php

/**
 * Digicrew Theme Setup and definitions
 * @package Digicrew
 */

function digicrew_change_permalinks() {
global $wp_rewrite;
$wp_rewrite->set_permalink_structure('/%postname%/');
$wp_rewrite->flush_rules();
}
add_action('after_switch_theme', 'digicrew_change_permalinks');

if ( ! function_exists( 'digicrew_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function digicrew_setup() {

		// Make theme available for translation.
		load_theme_textdomain( 'digicrew', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Custom Header
        add_theme_support("custom-header");

		// Let WordPress manage the document title.
		add_theme_support( 'title-tag' );

		add_theme_support( 'align-wide' );

		add_theme_support( 'editor-styles' );

		add_editor_style( 'style-editor.css' );

		// Enable support for Post Thumbnails on posts and pages.
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'digicrew' ),
			'footer'  => esc_html__( 'Footer Menu | depth 1', 'digicrew' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// add excerpt support for pages
		add_post_type_support( 'page', 'excerpt' );

		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'digicrew_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		/* Load editor style. */
		add_editor_style();

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/* Allow shortcodes in widgets. */
		add_filter( 'widget_text', 'do_shortcode' );
		 
		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		/* Change default image thumbnail sizes in WordPress */
		update_option( 'thumbnail_size_w', 300 );
		update_option( 'thumbnail_size_h', 300 );
		update_option( 'thumbnail_crop', 1 );
		update_option( 'medium_size_w', 455 );
		update_option( 'medium_size_h', 410 );
		update_option( 'medium_crop', 1 );
		update_option( 'large_size_w', 980 );
		update_option( 'large_size_h', 590 );
		update_option( 'large_crop', 1 );

		add_image_size( 'digicrew-case-slider',370,420,true);
		add_image_size( 'digicrew-case',350,397,true);
	}

endif;
add_action( 'after_setup_theme', 'digicrew_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function digicrew_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'digicrew_content_width', 640 );
}
add_action( 'after_setup_theme', 'digicrew_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function digicrew_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Blog', 'digicrew' ),
		'id'            => 'sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'digicrew' ),
		'before_widget' => '<div id="%1$s" class="widget sidebar-box %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="title">',
		'after_title'   => '</h3>',
	) );

	// Wigets for footer style 
	register_sidebar( array(
		'name' 			=> esc_html__( 'Footer Column 1', 'digicrew'),
		'id' 			=> 'digicrew-footer1',
		'description'   => esc_html__( 'Footer widget', 'digicrew'),
		'before_widget' => '<div id="%1$s" class="%2$s widget footer-box">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	));	

	register_sidebar( array(
		'name' 			=> esc_html__( 'Footer Column 2', 'digicrew'),
		'id' 			=> 'digicrew-footer2',
		'description'   => esc_html__( 'Footer widget', 'digicrew'),
		'before_widget' => '<div id="%1$s" class="%2$s widget footer-box">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	));	

	register_sidebar( array(
		'name' 			=> esc_html__( 'Footer Column 3', 'digicrew'),
		'id' 			=> 'digicrew-footer3',
		'description'   => esc_html__( 'Footer widget', 'digicrew'),
		'before_widget' => '<div id="%1$s" class="%2$s widget footer-box">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	));	
	register_sidebar( array(
		'name' 			=> esc_html__( 'Footer Column 4', 'digicrew'),
		'id' 			=> 'digicrew-footer4',
		'description'   => esc_html__( 'Footer widget', 'digicrew'),
		'before_widget' => '<div id="%1$s" class="%2$s widget footer-box">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	));	
}
add_action( 'widgets_init', 'digicrew_widgets_init' );

/**
 * Enqueue scripts and styles.
*/
function digicrew_scripts() {

	wp_dequeue_style( 'font-awesome' );
    wp_deregister_style( 'font-awesome' );
    
    // CSS Files
	wp_enqueue_style( 'google-fonts',          digicrew_fonts_url() ); 
	wp_enqueue_style( 'bootstrap',    		   get_template_directory_uri() . '/assets/css/bootstrap.min.css' ); 
	wp_enqueue_style( 'animate',               get_template_directory_uri() . '/assets/css/animate.min.css' );
	wp_enqueue_style( 'font-awesome',      	   get_template_directory_uri() . '/assets/css/all.min.css' ); 
	wp_enqueue_style( 'flaticon',              get_template_directory_uri() . '/assets/css/flaticon.css' ); 
	wp_enqueue_style( 'slicknav',              get_template_directory_uri() . '/assets/css/slicknav.min.css' );
	wp_enqueue_style( 'digicrew-slick-theme',  get_template_directory_uri() . '/assets/css/slick-theme.css' );
	wp_enqueue_style( 'digicrew-slick',        get_template_directory_uri() . '/assets/css/slick.css' ); 
	wp_enqueue_style( 'digicrew-pricing',      get_template_directory_uri() . '/assets/css/pricing.css' ); 
	wp_enqueue_style( 'lity',                  get_template_directory_uri() . '/assets/css/lity.min.css' ); 
	wp_enqueue_style( 'digicrew-custom-style', get_template_directory_uri() . '/assets/css/custom-style.css' );
	wp_enqueue_style( 'digicrew-responsive',   get_template_directory_uri() . '/assets/css/responsive.css' );
	wp_enqueue_style( 'digicrew-style',        get_stylesheet_uri() ); 
	
	// JS Files    
	wp_enqueue_script( 'bootstrap',        get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), true, true); 
	wp_enqueue_script( 'popper', 	       get_template_directory_uri() . '/assets/js/popper.min.js', array( 'jquery' ), true, true); 
    wp_enqueue_script( 'jquery-slicknav',  get_template_directory_uri() . '/assets/js/jquery.slicknav.min.js', array( 'jquery' ), true, true); 
    wp_enqueue_script( 'slick', 	       get_template_directory_uri() . '/assets/js/slick.min.js', array( 'jquery' ), true, true); 
	wp_enqueue_script( 'waypoints',        get_template_directory_uri() . '/assets/js/waypoints.min.js', array( 'jquery' ), true, true); 
	wp_enqueue_script( 'jquery-counterup', get_template_directory_uri() . '/assets/js/jquery.counterup.min.js', array( 'jquery' ), true, true); 
	wp_enqueue_script( 'digicrew-pricing', get_template_directory_uri() . '/assets/js/pricing.js', array( 'jquery' ), true, true); 
	wp_enqueue_script( 'lity', 	           get_template_directory_uri() . '/assets/js/lity.min.js', array( 'jquery' ), true, true); 
	wp_enqueue_script( 'isotope', 	       get_template_directory_uri() . '/assets/js/isotope.pkgd.min.js', array( 'jquery' ), true, true); 
	wp_enqueue_script( 'imagesloaded',     get_template_directory_uri() . '/assets/js/imagesloaded.js', array( 'jquery' ), true, true); 
    wp_enqueue_script( 'digicrew-custom',  get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery' ), true, true);

	$sticky_on = digicrew_get_opt( 'sticky_on' );

	$ajax_data = array(
        'sticky_on' => $sticky_on,
    );

	wp_enqueue_script( 'digicrew-ajax-front', get_template_directory_uri() . '/assets/js/digicrew-ajax-front.js', array( 'jquery' ), true, true );
    wp_localize_script( 'digicrew-ajax-front', 'ajax_admin', array(
            'ajax_url'    => admin_url( 'admin-ajax.php' ),
            'admin_nonce' => wp_create_nonce( 'admin_ajax_nonce' ),
            'ajax_data'   => $ajax_data,
    ) );
    
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'digicrew_scripts' );

/* add admin styles */
function digicrew_admin_style() {
	wp_enqueue_style( 'digicrew-meta-style',  get_template_directory_uri() . '/assets/css/digicrew-meta.css' );
	wp_enqueue_style( 'flaticon',             get_template_directory_uri() . '/assets/css/flaticon.css' ); 
	wp_enqueue_style( 'digicrew-admin-style', get_template_directory_uri() . '/assets/css/admin.css' );
}
add_action('admin_enqueue_scripts', 'digicrew_admin_style');

if ( ! function_exists( 'digicrew_fonts_url' ) ) :
    /**
     * Register Google fonts.
     *
     * Create your own digicrew_fonts_url() function to override in a child theme.
     *
     * @since league 1.1
     *
     * @return string Google fonts URL for the theme.
     */
    function digicrew_fonts_url()
    {
        $fonts_url = '';
        $fonts     = array();
        $subsets   = 'latin,latin-ext';

        if ( 'off' !== _x( 'on', 'Hind Siliguri font: on or off', 'digicrew' ) )
        {
            $fonts[] = 'Hind Siliguri:400,500,600';
        }

        if ( 'off' !== _x( 'on', 'Work Sans font: on or off', 'digicrew' ) )
        {
            $fonts[] = 'Work Sans:400,600,700,800';
        }

        if ( $fonts ) {
            $fonts_url = add_query_arg( array(
                'family' => urlencode( implode( '|', $fonts ) ),
                'subset' => urlencode( $subsets ),
            ), 'https://fonts.googleapis.com/css' );
        }
		return $fonts_url;
    }
endif;