<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Digicrew
 */
get_header(); ?>
<!-- error start -->
<section class="same-section-spacing">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="error-box">
                    <h1><?php esc_html_e( '404', 'digicrew' ); ?></h1>
                    <span><?php esc_html_e( 'OOOPS !', 'digicrew' ); ?></span>
                    <p><?php esc_html_e( 'Something Went Wrong. Go Back Home', 'digicrew' ); ?></p>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn-1"> <i class="fas fa-angle-double-left"></i><?php esc_html_e( 'Go Back', 'digicrew' ); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- error end-->
<?php get_footer(); ?>