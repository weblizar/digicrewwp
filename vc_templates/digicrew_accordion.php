<?php
extract( shortcode_atts( array(
    'accordion_item' => '',
    'el_class'       => '',

), $atts ) );

$accordion = (array) vc_param_group_parse_atts($accordion_item);


if ( ! empty( $accordion)) : ?>
    <div class="panel-group <?php echo esc_attr( $el_class ); ?>" id="accordion" role="tablist" aria-multiselectable="true">
        <?php 
        $i=1;
        foreach ( $accordion as $key => $value ) {

        $acc_title = isset( $value['title'] ) ? $value['title'] : '';
        $content   = isset( $value['content'] ) ? $value['content'] : '';
        ?>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading<?php echo esc_attr($i); ?>">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                            href="#collapse<?php echo esc_attr($i); ?>" aria-expanded="true" aria-controls="collapse<?php echo esc_attr($i); ?>">
                            <i class="more-less fas fa-angle-down"></i>
                            <span><?php echo esc_html($acc_title); ?></span>
                        </a>
                    </h4>
                </div>
                <div id="collapse<?php echo esc_attr($i); ?>" class="panel-collapse collapse <?php if($i=="1"){?>show<?php }?>" role="tabpanel"
                    aria-labelledby="heading<?php echo esc_attr($i); ?>">
                    <div class="panel-body">
                        <?php echo esc_html($content); ?>
                    </div>
                </div>
            </div>
        <?php 
        $i++;
        } ?>
    </div>
<?php endif; ?>