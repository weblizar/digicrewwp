<?php
extract( shortcode_atts( array(
    'port_order'   => 'DESC',
    'port_orderby' => 'date',
    'hide_empty'   => 0,
    'category'     => 'category',
    'el_class'     => '',
), $atts ) );


$n_category = explode( ',', $category );
$category_f = ( $category!=="all" ? $category : '' );

global $post;

$args = array(
    'post_type'      => 'portfolio',
    'order'          => $port_order,
    'orderby'        => $port_orderby,
    'posts_per_page' => 10,
    'post_status'    => 'publish'
);

if ( $category !== '' && $category !== "all" ) {
    $args['tax_query'] = array(
        array(
            'taxonomy'    => 'catportfolio',
            'field'       => 'slug',
            'post_status' => 'publish',
            'terms'       => $n_category
        )
    );
}

$port_query = new WP_Query( $args );
if ( $port_query->have_posts() ) : ?>
    
    <div class="port-slide <?php echo esc_attr( $el_class.' ' ); ?>">
        <?php  while ( $port_query->have_posts() ) : $port_query->the_post(); 
            $project_wrktyp = sanitize_text_field( get_post_meta( get_the_ID(), 'project_wrktyp', true ) );?>
            <div class="item">
                <div class="port-box">
                    <?php echo the_post_thumbnail('', array( 'class' => 'img-responsive' )); ?>
                    <div class="port-info">
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <p><?php echo esc_html( $project_wrktyp ); ?></p>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
    <!-- End-Case-listing -->
<?php endif; ?>