<?php
extract(shortcode_atts(array(
    'title'       => '',
    'currency'    => '',
    'price'       => '',
    'description' => '',
    'show_active' => '',
    'text_button' => '',
    'link_button' => '',
    'el_class'    => '',
), $atts ) );

$show_active = ( $show_active == true ? 'active' : ' ' );
$btn_link    = vc_build_link( $link_button );
$a_href      = '';
$a_target    = '_self';
if ( strlen( $btn_link['url'] ) > 0 ) {
    $a_href   = $btn_link['url'];
    $a_target = strlen( $btn_link['target'] ) > 0 ? $btn_link['target'] : '_self';
} 

$description = (array) vc_param_group_parse_atts( $description );
?>
<div class="pricing-wrapper <?php echo esc_attr( $el_class ); ?>">
    <div class="pricingTable <?php echo esc_attr( $show_active ); ?>">
        <div class="pricingTable-header">
            <span class="price-value"><?php echo esc_html( $currency . $price );?></span>
        </div>
        <h3 class="heading"><?php echo esc_html( $title ); ?></h3>
        <?php if ( ! empty( $description ) ) : ?>
            <div class="pricing-content">
                <ul>
                    <?php foreach ( $description as $key => $value ) {
                        $description_item = isset( $value['description_item'] ) ? $value['description_item'] : ''; ?>
                        <li><?php echo esc_html( $description_item ); ?></li>
                    <?php } ?>
                </ul>
            </div>
        <?php endif; 
        if ( ! empty ( $text_button ) ) : ?>
            <a href="<?php echo esc_url( $a_href );?>" class="read"><?php echo esc_html( $text_button );?></a>
        <?php endif; ?>
    </div>
</div>