<?php
extract( shortcode_atts( array(
    'title'         => '',
    'position'      => '',
    'image'         => '',
    'facebook_url'  => '',
    'twitter_url'   => '',
    'instagram_url' => '',
    'linkedin_url'  => '',
    'el_class'      => '',
), $atts ) );
$img_size  = "full";
$image     = wp_get_attachment_image_src( $image, $img_size );
$image_url = $image[0];
?>
<div class="team-box <?php echo esc_attr( $el_class.' ' ); ?>">
    <?php if ( $image_url ) { ?>
        <div class="img-wapper">
            <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( 'team-img' );?>">
        </div>
    <?php } ?>
    <div class="team-overlay">
        <div class="team-info">
            <h3><?php echo esc_html( $title ); ?></h3>
            <h4><?php echo esc_html( $position ); ?></h4>
            <ul class="social-icon">
                <?php if ( ! empty( $instagram_url ) ) { ?>
                    <li><a href="<?php echo esc_url( $instagram_url ); ?>"><i class="fab fa-instagram"></i></a></li>
                <?php } if ( ! empty( $twitter_url ) ) {?>
                    <li><a href="<?php echo esc_url( $twitter_url ); ?>"><i class="fab fa-twitter"></i></a></li>
                <?php } if ( ! empty( $facebook_url ) ) {?>
                    <li><a href="<?php echo esc_url( $facebook_url ); ?>"><i class="fab fa-facebook-f"></i></a></li>
                <?php } if ( ! empty( $linkedin_url ) ) { ?>
                    <li><a href="<?php echo esc_url( $linkedin_url ); ?>"><i class="fab fa-linkedin"></i></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>