<?php
extract( shortcode_atts( array(
    'testimonial_item' => '',
    'el_class'         => '',
), $atts) );

$testimonials = ( array ) vc_param_group_parse_atts( $testimonial_item );

if ( !empty( $testimonials ) ) : ?>
    <div class="row <?php echo esc_attr( $el_class ); ?>">
        <div class="col-lg-12">
            <div class="testi-slide testimonial-slider-content slider-for">
                <?php foreach ( $testimonials as $key => $value ) {
                $content = isset( $value['content'] ) ? $value['content'] : ''; ?>
                    <div class="item">
                        <p><?php echo esc_html( $content ); ?></p>
                    </div>
                <?php } ?>
            </div>
            
            <div class="testi-slide testimonial-slider-nav slider-nav">
                <?php foreach ( $testimonials as $key => $value ) {
                    $testi_title = isset( $value['title'] ) ? $value['title'] : '';
                    $position    = isset( $value['position'] ) ? $value['position'] : '';
                    $image       = isset( $value['image'] ) ? $value['image'] : '';
                    $img_size    = isset( $value['img_size'] ) ? $value['img_size'] : '200x200';
                    $img         = wpb_getImageBySize( array(
                        'attach_id'  => $image,
                        'thumb_size' => $img_size,
                        'class'      => '',
                    ));

                    if( isset ( $img['thumbnail'] ) && ! empty ( $img['thumbnail'] ) ) {
                        $thumbnail = $img['thumbnail'];
                    } else {
                        $thumbnail = '';
                    }

                    ?>
                    <div class="item">
                        <?php if ( !empty( $image ) ) :  ?>
                            <div class="img-nav">
                                <?php 
                                    echo wp_kses( 
                                        $thumbnail, 
                                        array(
                                        'img' => array(
                                            'class'  => array(),
                                            'src'    => array(),
                                            'width'  => array(),
                                            'height' => array(),
                                            'alt'    => array(),
                                            'title'  => array(),
                                        )
                                    ) ); 
                                ?>
                            </div>
                        <?php endif; ?>
                        <div class="testi-info">
                            <h3><?php echo esc_html( $testi_title ); ?></h3>
                            <h5><?php echo esc_html( $position ); ?></h5>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php endif; ?>