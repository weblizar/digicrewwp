<?php
extract( shortcode_atts( array(
    'address_title' => 'ADDRESS',
    'address_text'  => '',
    'phone_title'   => 'PHONE',
    'phone_no'      => '',
    'email_title'   => 'EMAIL',
    'email_text'    => '',
    'el_class'      => '',
), $atts ) );
?>
<div class="contact-inner <?php echo esc_attr($el_class.' '); ?>">
    <ul class="contact-info">
        <?php if(!empty($email_text)) : ?>
            <li><span><?php echo esc_html( $email_title ); ?></span> <?php echo esc_html( $email_text ); ?></li>
        <?php 
        endif;
        if(!empty($phone_no)) : ?>
            <li><span><?php echo esc_html( $phone_title ); ?></span><?php echo esc_html( $phone_no ); ?></li>
        <?php 
        endif;
        if(!empty($address_text)) : ?>
            <li><span><?php echo esc_html( $address_title ); ?></span> <?php echo esc_html( $address_text ); ?></li>
        <?php endif;?>
    </ul>
</div>