<?php

extract(shortcode_atts(array ( 
    'digit'      => '51 ',
    'title'      => 'Downloads ',
    'color'      => 'yellow' ,
    'el_class'   => '',
), $atts));
?>

<div class="counter-area">
    <div class="<?php echo esc_attr( $el_class ); ?>">
        <div class="inner-counter">
            <div class="counter-box <?php echo esc_attr( $color ); ?>">
                <span class="counter"><?php echo esc_html($digit); ?></span>
            </div>
            <?php if(!empty($title)) : ?>
                <h4><?php echo esc_html($title); ?></h4>
            <?php endif;?>
        </div>
    </div>
</div>