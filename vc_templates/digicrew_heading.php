<?php
extract(shortcode_atts(array(

    'style'         => 'center',
    'color'         => 'dark',
    'title'         => '',
    'description'   => '',
    'el_class'      => '',

), $atts));
if ( $style == 'center') : ?>
    <?php  if ( !empty( $title ) || !empty( $description ) ) : ?>
        <div class="main-title-two  <?php echo esc_attr($style) ;?> <?php echo esc_attr( $el_class ); ?>">
            <?php if( !empty( $title ) ) : ?>
                <h2 class="sec-title <?php echo esc_attr($color); ?>">
                    <?php echo esc_html( $title ); ?>
                </h2>
            <?php endif ; ?>
            <?php if( !empty( $description ) ) : ?>
                <h3 class="sub-title <?php echo esc_attr($color); ?>"><?php echo esc_html( $description ); ?></h3>
            <?php endif ; ?>
        </div>
    <?php  endif ;
elseif ( $style == 'left' || $style == 'right' ) :
    if ( !empty( $title ) ) : ?>
        <div class="left-main-title <?php echo esc_attr($style); ?> <?php echo esc_attr($color); ?> <?php echo esc_attr( $el_class ); ?>">
            <h2><?php echo esc_html( $title ); ?></h2>
        </div>
    <?php endif ; ?>
    <?php if ( !empty( $description ) ) : ?>
        <div class="fea-content <?php echo esc_attr( $style ); ?> <?php echo esc_attr( $color ); ?>">
            <p class="sub-title"><?php echo esc_html( $description ); ?></p>
        </div>
    <?php  endif;
endif ; ?>