<?php
    extract( shortcode_atts( array(
        'video_bg_image' => '',
        'video_link'     => '',
        'el_class'       => '',
    ), $atts ) );

    $image_url = '';

    if ( !empty( $atts['video_bg_image'] ) ) {
        $attachment_image = wp_get_attachment_image_src( $atts['video_bg_image'], 'full' );
        $image_url        = $attachment_image[0];
    }

    $btn_link = vc_build_link( $video_link );
    $a_href   = '';

    if ( strlen( $btn_link['url'] ) > 0 ) {
        $a_href = $btn_link['url'];
    }

    if ( ! empty( $video_link ) ) : 
?>
    <div class="<?php echo esc_attr( $el_class ); ?>">
        <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( 'ellipse' );?>">
        <div class="video-btn">
            <a href="<?php echo esc_url( $a_href );?>" data-lity><i class="fas fa-play"></i></a>
        </div>
    </div>
<?php endif; ?>