<?php
extract( shortcode_atts( array(
    'button_text'  => '',
    'button_link'  => '',
    'align_lg'     => 'left',
    'text_color'   => '',
    'bg_color'     => '',
    'el_class'     => '',
    'add_button'   => '',
    'button_text2' => '',
    'button_link2' => '',
    'text_color2'  => '',
    'bg_color2'    => '',
    'el_class2'    => ''

), $atts ) );

$btn_link = vc_build_link($button_link);
$a_href = '';
$a_target = '';
if ( strlen( $btn_link['url'] ) > 0 ) {
    $a_href   = 'href="'.esc_url($btn_link['url']).'"';
    $a_target = strlen( $btn_link['target'] ) > 0 ? 'target="'.esc_attr($btn_link['target']).'"' : 'target="_self"';
}

$styles1 = array();

$button_inline_style1 = '';
if( !empty($text_color) ) {
    $styles1[] .= 'color:'.esc_attr($text_color);
}
if( !empty($bg_color) ) {
    $styles1[] .= 'background:'.esc_attr($bg_color);
    $styles1[] .= 'border: solid 1px '.esc_attr($bg_color) .'!important';
}
if (!empty($styles1)) {
    $button_inline_style1 = 'style="' . esc_attr( implode( ';', $styles1 ) ) . '"';
}

$wrapper_id = abs( crc32( uniqid() ) );

if ( $add_button == true ) {

    $link2 = vc_build_link($button_link2);
    $a_href2 = '';
    $a_target2 = '';
    if ( strlen( $link2['url'] ) > 0 ) {
        $a_href2   = 'href="'.esc_url($link2['url']).'"';
        $a_target2 = strlen( $link2['target'] ) > 0 ? 'target="'.esc_attr($link2['target']).'"' : 'target="_self"';
    }
    
    $styles2 = array();
    $button_inline_style2 = '';
    if( !empty($text_color2) ) {
        $styles2[] .= 'color:'.esc_attr($text_color2);
    }
    if( !empty($bg_color2) ) {
        $styles2[] .= 'background:'.esc_attr($bg_color2);
        $styles2[] .= 'border: solid 1px '.esc_attr($bg_color2).'!important';
    }

    if (!empty($styles2)) {
        $button_inline_style2 = 'style="' . esc_attr( implode( ';', $styles2 ) ) . '"';
    }
}
?>

<div class="btn-box" <?php echo !empty($align_lg) ? 'style="text-align:'.esc_attr($align_lg).'"' : '' ?>>
    
    <a <?php printf( wp_kses( '%1$s', array( 'href' => array() ) ), $a_href ); ?> data-txt="<?php echo esc_attr( $text_color );?>"  data-bg="<?php echo esc_attr( $bg_color );?>"  <?php printf( wp_kses( '%1$s', array( 'target' => array() ) ), $a_target ); ?> class="btn-1 <?php if ( $add_button == true ) { ?>mr-4 <?php } ?><?php echo esc_attr( $el_class ); ?>" <?php printf( wp_kses( '%1$s', array( 'style' => array() ) ), $button_inline_style1 ); ?>>
        <?php echo esc_html( $button_text ); ?>
    </a>

    <?php if ( $add_button == true ) { ?>
        <a <?php printf( wp_kses( '%1$s', array( 'href' => array() ) ), $a_href2 ); ?> data-txt="<?php echo esc_attr( $text_color2 );?>"  data-bg="<?php echo esc_attr( $bg_color2 );?>"  <?php printf( wp_kses( '%1$s', array( 'target' => array() ) ), $a_target2 ); ?> class="btn-1 <?php echo esc_attr( $el_class2 ); ?>" <?php printf( wp_kses( '%1$s', array( 'style' => array() ) ), $button_inline_style2 ); ?>>
            <?php echo esc_html( $button_text2 ); ?> 
        </a>
    <?php } ?>
</div>