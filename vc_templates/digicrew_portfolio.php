<?php
extract( shortcode_atts( array(
    'hide_empty'     => 0,
    'port_layout'    => 'col-lg-4',
    'port_order'     => 'DESC',
    'port_orderby'   => 'date',
    'posts_per_page' => '9',
    'filter'         => '',
    'pagination'     => '',
    'category'       => 'category',
    'el_class'       => '',
), $atts ) );

if ( get_query_var('paged') ) { $port_paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $port_paged = get_query_var('page'); }
else { $port_paged = 1; }

$n_category = explode(',', $category);
$category_f = ( $category!=="all" ? $category : '');

global $post;

$args = array(
    'post_type'      => 'portfolio',
    'order'          => $port_order,
    'orderby'        => $port_orderby,
    'posts_per_page' => $posts_per_page,
    'paged'          => $port_paged,
    'post_status'    => 'publish'
);

if ( $category !== '' && $category !== "all" ) {
    $args['tax_query'] = array(
        array(
            'taxonomy'    => 'catportfolio',
            'field'       => 'slug',
            'post_status' => 'publish',
            'terms'       => $n_category
        )
    );
}

$port_query = new WP_Query( $args );
if ( $port_query->have_posts() ) : ?>
    
    <!-- Case-listing -->
    <div class="case-listing <?php echo esc_attr( $el_class.' ' ); ?>">
        <?php if ( $filter !== '') { ?>
            <div class="row">
                <div class="col-lg-12 text-right">
                    <?php digicrew_showPortCategory( $n_category );  ?>
                </div>
            </div>
        <?php } ?>
        <div class="row isotop-grid">
            <?php 
            while ( $port_query->have_posts() ) : $port_query->the_post();
                $project_wrktyp      = sanitize_text_field( get_post_meta( get_the_ID(), 'project_wrktyp', true ) );
                

                if ( ! isset( $echoallterm ) ) {$echoallterm = ''; $showterm = '';}
                $new_term_list = get_the_terms( get_the_id(), "catportfolio" );

                if ( is_array( $new_term_list ) ) {
                    foreach ( $new_term_list as $term_port ) {
                        $tempname = strtr( $term_port->name, array(
                        ' ' => '-',
                        ) );
                        $echoallterm .= $term_port->slug . " ";
                        $echoterm    = $term_port->name;
                    }

                    foreach ( $new_term_list as $term_port ) {
                        $showterm .= strtolower($term_port->name) . " ";
                    }
                }
                ?>
                <div class="isotop-item <?php echo esc_attr( $port_layout ); ?> col-md-6 col-sm-12 <?php echo esc_attr( $echoallterm );?>">
                    <div class="port-box">
                        <?php echo the_post_thumbnail('digicrew-case', array( 'class' => 'img-responsive' )); ?>
                        <div class="port-info">
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <p><?php echo esc_html( $project_wrktyp ); ?></p>
                        </div>
                    </div>
                </div>
            <?php 
            unset( $echoallterm );
        endwhile; ?>
        </div>
        <?php if ( $pagination !== '' ) { ?>
            <!-- Pagination -->
            <?php digicrew_theme_pagination( $port_query ); ?>
        <?php  } ?>
    </div>
<!-- End-Case-listing -->
<?php endif; ?>