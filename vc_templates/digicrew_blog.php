<?php
extract(shortcode_atts(array(
    'bg_column'        => 'col-lg-12',
    'category'         => '',
    'to_show'          => 5,
    'excerpt_length'   => 50,
    'blog_order'       => '',
    'blog_orderby'     => '',
    'pagination'       => '',
    'excerpt'          => '',
    'readmore'         => '',
    'el_class'         => '',
), $atts));

$category_f = ( $category!=="all" ? $category : '');

if( !empty( $excerpt_length ) ) { $blog_exceprt = $excerpt_length; }
else{ $blog_exceprt = 20; }


if ( get_query_var('paged') ) { $blog_paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $blog_paged = get_query_var('page'); }
else { $blog_paged = 1; }

$query = new WP_Query();

if ( $category_f !== '' ) {
	$query->query(array(
        'post_type'           => 'post',
        'order'               => $blog_order,
        'orderby'             => $blog_orderby,
        'category_name'       => $category_f,
        'posts_per_page'      => $to_show,
        'paged'               => $blog_paged,
        'post_status'         => 'publish',
        'ignore_sticky_posts' => 1,   
	) );
} else {
	$query->query(array(
        'post_type'           => 'post',
        'order'               => $blog_order,
        'orderby'             => $blog_orderby,
        'paged'               => $blog_paged,
        'posts_per_page'      => $to_show,
        'post_status'         => 'publish',
        'ignore_sticky_posts' => 1,
	) );
} ?>
<div class="row <?php echo esc_attr($el_class.' '); ?>">
    <?php while ( $query->have_posts() ) : $query->the_post();?>
        <div class="<?php echo esc_attr($bg_column); ?>">
            <div class="blog-box blog-single-box <?php if ( has_post_thumbnail() ) :?>blog-img<?php endif ; ?>">
                <?php if ( has_post_thumbnail() ) : ?>    
                    <div class="wapper-img">
                        <?php the_post_thumbnail( array( 570, 365 ) ); ?>
                    </div>
                <?php endif ; ?>
                <div class="blog-content">
                    <?php digicrew_archive_meta(); ?>
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    
                    <?php if ( $excerpt !== '') { ?>
                        <p><?php echo digicrew_custom_excerpt( $blog_exceprt ); ?></p>
                    <?php } if ( $readmore !== '') { ?>
                        <a href="<?php the_permalink(); ?>" class="read-btn">
                            <?php esc_html_e( 'Read More', 'digicrew' );?> <i class="fas fa-angle-double-right"></i>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
    <!-- Pagination -->
</div>
<?php if ( $pagination !== '') { 
    digicrew_theme_pagination($query); 
} ?>