<?php
extract(shortcode_atts(array(
    'feature_item' => '',
    'el_class'     => '',
), $atts));

$feature_item = (array) vc_param_group_parse_atts($feature_item);
if(!empty($feature_item)) :?>
    <div class="icon-slide <?php echo esc_attr( $el_class ); ?>">
        <?php foreach ($feature_item as $key => $value) {
            $slider_title     = isset($value['title']) ? $value['title'] : '';
            $icon_type        = isset($value['icon_type']) ? $value['icon_type'] : '';
            $icon_list        = isset($value['icon_list']) ? $value['icon_list'] : '';
            $icon_flaticon    = isset($value['icon_flaticon']) ? $value['icon_flaticon'] : '';
            $icon_fontawesome = isset($value['icon_fontawesome']) ? $value['icon_fontawesome'] : '';
            $icon_name        = "icon_" . $icon_list;
            $icon_class       = isset(${$icon_name}) ? ${$icon_name} : '';
            ?>
            <div class="item">
                <div class="icon-box">
                    <span class="icon-box <?php echo esc_attr($icon_class); ?>"></span>
                    <h3><?php echo esc_html($slider_title); ?></h3>
                </div>
            </div>
        <?php } ?>
    </div>
<?php endif; ?>