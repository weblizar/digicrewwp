<?php
extract( shortcode_atts( array(
    'style'            => 'style-1',
    'title'            => '',
    'description'      => '',
    'icon_type'        => 'icon',
    'icon_list'        => 'fontawesome',
    'icon_fontawesome' => '',
    'icon_flaticon'    => '',
    'icon_bg'          => 'pink',
    'el_class'         => '',
), $atts ) );
$icon_name  = "icon_" . $icon_list;
$icon_class = isset( ${$icon_name} ) ? ${$icon_name} : '';

if ( $style == 'style-1') : 
    $div_class = "service-center-with-icon"; 
    $icon      = "icon-box" ;

elseif ( $style == 'style-2' ) : 
    $div_class = "service-wt-bor"; 
    $icon      = "icon-two" ;

elseif ( $style == 'style-3' ) : 
    $div_class = "service-wt-bor text-left bor-none p-0"; 
    $icon      = "icon-two" ;
endif ; ?>

<div class="<?php echo esc_attr( $div_class ); ?> <?php echo esc_attr( $el_class ); ?>">
    <?php if ( $icon_class ):?>
        <span class="<?php echo esc_attr( $icon );?> <?php echo esc_attr( $icon_bg ); ?>"><i class="<?php echo esc_attr( $icon_class ); ?>"></i></span>
    <?php endif; 
    if ( ! empty ( $title ) ) : ?>
        <h3><?php echo esc_html( $title ); ?></h3>
    <?php endif;
    if ( ! empty ( $description ) ) : ?>
        <p><?php echo esc_html( $description ); ?></p>
    <?php endif;?> 
</div>