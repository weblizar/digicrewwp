<?php
extract( shortcode_atts( array(
    'timeline_item' => '',
    'el_class'      => '',
), $atts ) );

$timeline = ( array ) vc_param_group_parse_atts( $timeline_item );

if ( !empty( $timeline ) ) : ?>
    <div class="main-timeline <?php echo esc_attr( $el_class ); ?>">
        <?php foreach ( $timeline as $key => $value ) {
            $timeline_title = isset( $value['title'] ) ? $value['title'] : '';
            $content        = isset ( $value['content'] ) ? $value['content'] : ''; 
        ?>
            <div class="timeline">
                <div class="timeline-content">
                    <h3 class="title"><?php echo esc_html( $timeline_title ); ?></h3>
                    <p class="description"><?php echo esc_html( $content ); ?></p>
                </div>
            </div>
        <?php } ?>
    </div>
<?php endif; ?>