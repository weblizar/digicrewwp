<?php
extract( shortcode_atts( array(
    'heading'     => '',
    'heading_2'   => '',
    'sub_heading' => '',
    'bg_image'    => '',
    'ryt_image'   => '',
    'btn_txt_1'   => '',
    'btn_url_1'   => '',
    'btn_txt_2'   => '',
    'btn_url_2'   => '',
    'el_class'    => '',
), $atts ) );
$img_size      = "full";
$bg_image      = wp_get_attachment_image_src( $bg_image, $img_size );
$bg_image_url  = $bg_image[0];
$ryt_image     = wp_get_attachment_image_src( $ryt_image, $img_size );
$ryt_image_url = $ryt_image[0];
?>
<!-- Banner-start -->
<div class="hero-area" <?php echo esc_attr( $el_class ); ?> style="background-image:url('<?php echo esc_url( $bg_image_url );?>')">
    <div class="slide-table">
        <div class="slide-table-cell">
            <div class="container">
                <div class="row">
                    <div class="<?php if ( !empty ( $ryt_image_url ) ) { ?> col-lg-8 <?php } else { ?> col-md-12 <?php } ?> col-sm-12">
                        <div class="slide-content">
                            <h4><?php echo esc_html( $heading ); ?></h4>
                            <h2><?php echo esc_html( $heading_2 ); ?></h2>
                            <p><?php echo esc_html( $sub_heading ); ?></p>
                            <?php if ( ! empty( $btn_url_1 ) && ! empty ( $btn_txt_1 ) ) { ?>
                                <a href="<?php echo esc_url( $btn_url_1 ); ?>" class="btn btn-theme btn-active"><?php echo esc_html( $btn_txt_1 ); ?></a>
                            <?php } ?>
                            <?php if ( ! empty( $btn_url_2 ) && ! empty ( $btn_txt_2 ) ) { ?>
                                <a href="<?php echo esc_url( $btn_url_2 ); ?>" class="btn btn-theme"><?php echo esc_html( $btn_txt_2 ); ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php if ( ! empty ( $ryt_image_url ) ) { ?>
                    <div class="right-img-box">
                        <img src="<?php echo esc_url( $ryt_image_url ); ?>" alt="<?php echo esc_attr( 'slide-img' );?>">
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- Shape -->
    <div class="shapes">
        <svg class="abstract-svg-1" viewBox="0 0 102 102">
            <circle cx="50" cy="50" r="50"></circle>
        </svg>
        <svg class="abstract-svg-2" viewBox="0 0 102 102">
            <circle cx="50" cy="50" r="50"></circle>
        </svg>
        <svg class="abstract-svg-3" viewBox="0 0 102 102">
            <circle cx="50" cy="50" r="50"></circle>
        </svg>
        <svg class="abstract-svg-4" viewBox="0 0 102 102">
            <circle cx="50" cy="50" r="50"></circle>
        </svg>
        <svg class="abstract-svg-5" viewBox="0 0 102 102">
            <circle cx="50" cy="50" r="50"></circle>
        </svg>
        <svg class="abstract-svg-6" viewBox="0 0 102 102">
            <circle cx="50" cy="50" r="50"></circle>
        </svg>
    </div>
</div>
<!-- Banner-end -->