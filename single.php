<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Digicrew
*/
get_header();
$sidebar_pos = digicrew_get_opt( 'post_sidebar_pos', 'right' );
the_post(); 
?>
<!-- Single-Blog -->
<section class="same-section-spacing single-blog">
    <div class="container">
        <div class="row">
            <div <?php digicrew_primary_class( $sidebar_pos, '' ); ?>>
                <?php get_template_part( 'template-parts/content','single' ); ?>
            </div>
            <?php if ( 'none' != $sidebar_pos ) : ?>
                <!-- Side-bar -->
                <div <?php digicrew_secondary_class( $sidebar_pos, '' ); ?>>
                    <?php get_sidebar(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<!-- End-Single-Blog -->
<?php get_footer(); ?>